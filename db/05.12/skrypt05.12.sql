CREATE TABLE Users (
  id_users INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  grupa INTEGER UNSIGNED NOT NULL,
  Imie VARCHAR(255) NULL,
  Nazwisko VARCHAR(255) NULL,
  haslo VARCHAR(255) NULL,
  email VARCHAR(255) NULL,
  data_rejestracji TIMESTAMP NULL,
  role VARCHAR(255) NULL,
  login VARCHAR(255) NULL,
  stan VARCHAR(255) NULL,
  PRIMARY KEY(id_users)
  FOREIGN KEY (grupa) REFERENCES grupa (id_grupa) on delete cascade
);
CREATE TABLE Role (
  id_role INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_testera INTEGER UNSIGNED NOT NULL,
  id_dydaktyka INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(id_role),
  FOREIGN KEY (id_testera) REFERENCES Users (id_users) on delete cascade,
  FOREIGN KEY (id_dydaktyka) REFERENCES Users (id_users) on delete cascade
);
CREATE TABLE Test (
  id_test INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_dydaktyka INTEGER UNSIGNED NOT NULL,
  nazwa_testu VARCHAR(255) NULL,
  opis_testu VARCHAR(255) NULL,
  stan VARCHAR(255) NULL,
  czas_trwania INTEGER UNSIGNED NULL,
  PRIMARY KEY(id_test),
  FOREIGN KEY (id_dydaktyka) REFERENCES Users (id_users) on delete cascade
);
CREATE TABLE Ankiety (
  id_ankiety INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_autor INTEGER UNSIGNED NOT NULL,
  nazwa_ankiety VARCHAR(255) NULL,
  stan VARCHAR(1) NULL,
  PRIMARY KEY(id_ankiety),
  FOREIGN KEY (id_autor) REFERENCES users (id_users) on delete cascade
);
CREATE TABLE Pytania (
  id_pytanie INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_test INTEGER UNSIGNED NOT NULL,
  nazwa_pytania TEXT NULL,
  PRIMARY KEY(id_pytanie),
  FOREIGN KEY (id_test) REFERENCES Test (id_test) on delete cascade
);
CREATE TABLE Pytania_Ankiety (
  id_pytania_ankiety INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_ankiety INTEGER UNSIGNED NULL,
  nazwa_pytania TEXT NULL,
  PRIMARY KEY(id_pytania_ankiety),
  FOREIGN KEY (id_ankiety) REFERENCES Ankiety (id_ankiety) on delete cascade
);
CREATE TABLE Odpowiedz (
  id_odpowiedz INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_pytanie INTEGER UNSIGNED NOT NULL,
  tresc_odpowiedzi LONGTEXT NULL,
  ilosc_punktow INTEGER UNSIGNED NULL,
  PRIMARY KEY(id_odpowiedz),
  FOREIGN KEY (id_pytanie) REFERENCES Pytania (id_pytanie) on delete cascade
);

CREATE TABLE Odpowiedz_Ankiety (
  id_odpowiedz_ankiety INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_pytania_ankiety INTEGER UNSIGNED NOT NULL,
  tresc_odpowiedzi TEXT NULL,
  ilosc_klikniec INTEGER UNSIGNED NULL,
  PRIMARY KEY(id_odpowiedz_ankiety),
  FOREIGN KEY (id_pytania_ankiety) REFERENCES Pytania_Ankiety (id_pytania_ankiety) on delete cascade
);

CREATE TABLE Wyniki (
  id_wyniki INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_test INTEGER UNSIGNED NOT NULL,
  id_testera INTEGER UNSIGNED NOT NULL,
  zdobyty_wynik INTEGER UNSIGNED NOT NULL,
  data_start TIMESTAMP NULL,
  data_stop TIMESTAMP NULL,
  PRIMARY KEY(id_wyniki),
  FOREIGN KEY (id_test) REFERENCES Test (id_test) on delete cascade,
  FOREIGN KEY (id_testera) REFERENCES Users (id_users) on delete cascade
);
CREATE TABLE TagiAnkiety (
  id_Tagi_Ankiety INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_ankiety INTEGER UNSIGNED NOT NULL,
  nazwa VARCHAR(255) NULL,
  PRIMARY KEY(id_Tagi_Ankiety),
  FOREIGN KEY (id_ankiety) REFERENCES Ankiety (id_ankiety) on delete cascade
);
CREATE TABLE TagiTest (
  id_tagi_test INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_test INTEGER UNSIGNED NOT NULL,
  nazwa VARCHAR(255) NULL,
  PRIMARY KEY(id_tagi_test),
  FOREIGN KEY (id_test) REFERENCES Test (id_test) on delete cascade
);
CREATE TABLE klasy (
  id_klasy INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_dydaktyka INTEGER UNSIGNED NOT NULL,
  nazwa VARCHAR(255) NULL,
  PRIMARY KEY(id_tagi_test),
  FOREIGN KEY (id_dydaktyka) REFERENCES users (id_users) on delete cascade
);
CREATE TABLE grupy (
  id_grupy INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_klasy INTEGER UNSIGNED NOT NULL,
  nazwa VARCHAR(255) NULL,
  PRIMARY KEY(id_grupy),
  FOREIGN KEY (id_klasy) REFERENCES klasy (id_klasy) on delete cascade
);

