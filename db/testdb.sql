-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas wygenerowania: 28 May 2012, 12:17
-- Wersja serwera: 5.5.16
-- Wersja PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `testdb`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `ankiety`
--

CREATE TABLE IF NOT EXISTS `ankiety` (
  `id_ankiety` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_autor` int(10) unsigned NOT NULL,
  `nazwa_ankiety` varchar(255) DEFAULT NULL,
  `stan` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_ankiety`),
  KEY `id_autor` (`id_autor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `ankiety`
--

INSERT INTO `ankiety` (`id_ankiety`, `id_autor`, `nazwa_ankiety`, `stan`) VALUES
(1, 1, 'asssssssss', 'N'),
(2, 1, 'aaaaaaaa', 'N'),
(3, 1, '', 'N');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `grupy`
--

CREATE TABLE IF NOT EXISTS `grupy` (
  `id_grupy` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_klasy` int(10) unsigned NOT NULL,
  `nazwa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_grupy`),
  KEY `id_klasy` (`id_klasy`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Zrzut danych tabeli `grupy`
--

INSERT INTO `grupy` (`id_grupy`, `id_klasy`, `nazwa`) VALUES
(3, 9, 'grupa1'),
(4, 9, 'grupa2'),
(5, 9, 'grupa3'),
(6, 10, 'bleee'),
(7, 11, 'grupa1'),
(8, 12, 'twoja grupa'),
(9, 13, 'grupa1'),
(10, 13, 'grupa2'),
(11, 13, 'grupa3'),
(12, 13, 'grupa4'),
(13, 14, 'asdasd'),
(14, 14, 'asdasdasd');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `klasy`
--

CREATE TABLE IF NOT EXISTS `klasy` (
  `id_klasy` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_dydaktyka` int(10) unsigned NOT NULL,
  `nazwa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_klasy`),
  KEY `id_dydaktyka` (`id_dydaktyka`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Zrzut danych tabeli `klasy`
--

INSERT INTO `klasy` (`id_klasy`, `id_dydaktyka`, `nazwa`) VALUES
(9, 1, 'naszaklasa'),
(10, 1, 'wyswietlanie danych'),
(11, 1, 'moja klasa'),
(12, 1, 'twoja klasa'),
(13, 1, 'zzz'),
(14, 1, 'asdasdasdasd');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `mail_token`
--

CREATE TABLE IF NOT EXISTS `mail_token` (
  `id_mail_token` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_users` int(10) unsigned NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `data_wysłania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_mail_token`),
  KEY `id_users` (`id_users`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `odpowiedz`
--

CREATE TABLE IF NOT EXISTS `odpowiedz` (
  `id_odpowiedz` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pytanie` int(10) unsigned NOT NULL,
  `tresc_odpowiedzi` longtext,
  `ilosc_punktow` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_odpowiedz`),
  KEY `id_pytanie` (`id_pytanie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Zrzut danych tabeli `odpowiedz`
--

INSERT INTO `odpowiedz` (`id_odpowiedz`, `id_pytanie`, `tresc_odpowiedzi`, `ilosc_punktow`) VALUES
(1, 1, NULL, 1),
(2, 2, NULL, 1),
(3, 3, NULL, 1),
(4, 6, NULL, 1),
(5, 7, NULL, 2),
(6, 8, NULL, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `odpowiedz_ankiety`
--

CREATE TABLE IF NOT EXISTS `odpowiedz_ankiety` (
  `id_odpowiedz_ankiety` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pytania_ankiety` int(10) unsigned NOT NULL,
  `tresc_odpowiedzi` text,
  `ilosc_klikniec` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_odpowiedz_ankiety`),
  KEY `id_pytania_ankiety` (`id_pytania_ankiety`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `odpowiedz_ankiety`
--

INSERT INTO `odpowiedz_ankiety` (`id_odpowiedz_ankiety`, `id_pytania_ankiety`, `tresc_odpowiedzi`, `ilosc_klikniec`) VALUES
(1, 1, NULL, 0),
(2, 2, NULL, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `pytania`
--

CREATE TABLE IF NOT EXISTS `pytania` (
  `id_pytanie` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_test` int(10) unsigned NOT NULL,
  `nazwa_pytania` text,
  PRIMARY KEY (`id_pytanie`),
  KEY `id_test` (`id_test`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `pytania` (`id_pytanie`, `id_test`, `nazwa_pytania`) VALUES
(1, 5, 'sdfsfsf'),
(2, 5, 'sdfsdfsfs'),
(3, 5, 'sdfsdfsf'),
(4, 6, ''),
(5, 6, ''),
(6, 7, ''),
(7, 7, ''),
(8, 7, 'sdfsdf');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `pytania_ankiety`
--

CREATE TABLE IF NOT EXISTS `pytania_ankiety` (
  `id_pytania_ankiety` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_ankiety` int(10) unsigned DEFAULT NULL,
  `nazwa_pytania` text,
  PRIMARY KEY (`id_pytania_ankiety`),
  KEY `id_ankiety` (`id_ankiety`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `pytania_ankiety`
--

INSERT INTO `pytania_ankiety` (`id_pytania_ankiety`, `id_ankiety`, `nazwa_pytania`) VALUES
(1, 2, 'aaaaaaaaaaaa'),
(2, 3, '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id_role` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_testera` int(10) unsigned NOT NULL,
  `id_dydaktyka` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_role`),
  KEY `id_testera` (`id_testera`),
  KEY `id_dydaktyka` (`id_dydaktyka`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `role`
--

INSERT INTO `role` (`id_role`, `id_testera`, `id_dydaktyka`) VALUES
(1, 4, 1),
(2, 5, 1),
(4, 7, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `tagi_ankiety`
--

CREATE TABLE IF NOT EXISTS `tagi_ankiety` (
  `id_tagi_ankiety` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_ankiety` int(10) unsigned NOT NULL,
  `nazwa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tagi_ankiety`),
  KEY `id_ankiety` (`id_ankiety`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `tagi_test`
--

CREATE TABLE IF NOT EXISTS `tagi_test` (
  `id_tagi_test` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_test` int(10) unsigned NOT NULL,
  `nazwa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tagi_test`),
  KEY `id_test` (`id_test`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id_test` int(10) unsigned NOT NULL,
  `id_dydaktyka` int(10) unsigned NOT NULL,
  `nazwa_testu` varchar(255) DEFAULT NULL,
  `opis_testu` varchar(255) DEFAULT NULL,
  `stan` varchar(255) DEFAULT NULL,
  `czas_trwania` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_test`),
  KEY `id_dydaktyka` (`id_dydaktyka`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `test`
--

INSERT INTO `test` (`id_test`, `id_dydaktyka`, `nazwa_testu`, `opis_testu`, `stan`, `czas_trwania`) VALUES
(1, 1, 'adasd', 'adasdasd', 'N', NULL),
(2, 1, 'testyyyyyyyyy', 'testyyyyyyyyy', 'N', NULL),
(3, 1, 'testyyy222222222', 'testyyyyyyyy2', 'N', NULL),
(4, 1, 'testyyy222222222', 'testyyyyyyyy3', 'N', NULL),
(5, 1, '', '', 'A', NULL),
(6, 1, '', '', 'N', NULL),
(7, 1, '', '', 'N', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `tester_grupa`
--

CREATE TABLE IF NOT EXISTS `tester_grupa` (
  `id_tester_grupa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_testera` int(10) unsigned NOT NULL,
  `id_grupy` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_tester_grupa`),
  KEY `id_testera` (`id_testera`),
  KEY `id_grupy` (`id_grupy`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `tester_grupa`
--

INSERT INTO `tester_grupa` (`id_tester_grupa`, `id_testera`, `id_grupy`) VALUES
(1, 7, 9);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_users` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imie` varchar(255) DEFAULT NULL,
  `nazwisko` varchar(255) DEFAULT NULL,
  `haslo` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `data_rejestracji` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `role` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `stan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id_users`, `imie`, `nazwisko`, `haslo`, `email`, `data_rejestracji`, `role`, `login`, `stan`) VALUES
(1, 'wojtek', 'koszycki', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.pl', NULL, 'admin', 'admin', 'N'),
(3, 'user', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user@user.pl', '2012-05-17 14:05:19', 'user', 'user', 'N'),
(4, 'test', 'test', '5a105e8b9d40e1329780d62ea2265d8a', 'sdfsdf@o2.pl', '2012-05-20 14:57:46', 'user', 'test1', 'N'),
(5, 'test', 'test', 'ad0234829205b9033196ba818f7a872b', 'sdfsdf@o2.pl', '2012-05-20 15:01:22', 'user', 'test2', 'N'),
(6, 'uczen', 'uczen', 'd0bc4421f0910959b65e9c80aa4746c1', 'uczen@uczen.pl', '2012-05-26 10:44:25', 'user', 'uczen1', 'N'),
(7, 'lolek', 'bolek', '486d9ff14291c7e88d9e69147c1a351f', 'bolek@bolek.pl', '2012-05-26 11:05:29', 'user', 'bolek', 'N'),
(15, 'bolek', 'bolek', '486d9ff14291c7e88d9e69147c1a351f', 'lolek@lolek.pl', '2012-05-26 21:53:44', 'user', 'bolek', 'N'),
(16, 'bolek', 'bolek', '207023ccb44feb4d7dadca005ce29a64', 'lolek@lolek.pl', '2012-05-26 21:54:58', 'user', 'bolek', 'N'),
(18, 'bolek', 'bolek', '486d9ff14291c7e88d9e69147c1a351f', 'bolek@bolek.pl', '2012-05-27 11:40:38', 'user', 'bolek', 'N');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `wyniki`
--

CREATE TABLE IF NOT EXISTS `wyniki` (
  `id_wyniki` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_test` int(10) unsigned NOT NULL,
  `id_testera` int(10) unsigned NOT NULL,
  `zdobyty_wynik` int(10) unsigned NOT NULL,
  `data_start` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_stop` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_wyniki`),
  KEY `id_test` (`id_test`),
  KEY `id_testera` (`id_testera`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `wyniki`
--

INSERT INTO `wyniki` (`id_wyniki`, `id_test`, `id_testera`, `zdobyty_wynik`, `data_start`, `data_stop`) VALUES
(1, 5, 7, 0, '2012-05-27 18:05:42', NULL);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `ankiety`
--
ALTER TABLE `ankiety`
  ADD CONSTRAINT `ankiety_ibfk_1` FOREIGN KEY (`id_autor`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `grupy`
--
ALTER TABLE `grupy`
  ADD CONSTRAINT `grupy_ibfk_1` FOREIGN KEY (`id_klasy`) REFERENCES `klasy` (`id_klasy`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `klasy`
--
ALTER TABLE `klasy`
  ADD CONSTRAINT `klasy_ibfk_1` FOREIGN KEY (`id_dydaktyka`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `mail_token`
--
ALTER TABLE `mail_token`
  ADD CONSTRAINT `mail_token_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `odpowiedz`
--
ALTER TABLE `odpowiedz`
  ADD CONSTRAINT `odpowiedz_ibfk_1` FOREIGN KEY (`id_pytanie`) REFERENCES `pytania` (`id_pytanie`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `odpowiedz_ankiety`
--
ALTER TABLE `odpowiedz_ankiety`
  ADD CONSTRAINT `odpowiedz_ankiety_ibfk_1` FOREIGN KEY (`id_pytania_ankiety`) REFERENCES `pytania_ankiety` (`id_pytania_ankiety`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `pytania`
--
ALTER TABLE `pytania`
  ADD CONSTRAINT `pytania_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `test` (`id_test`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `pytania_ankiety`
--
ALTER TABLE `pytania_ankiety`
  ADD CONSTRAINT `pytania_ankiety_ibfk_1` FOREIGN KEY (`id_ankiety`) REFERENCES `ankiety` (`id_ankiety`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `role`
--
ALTER TABLE `role`
  ADD CONSTRAINT `role_ibfk_1` FOREIGN KEY (`id_testera`) REFERENCES `users` (`id_users`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_ibfk_2` FOREIGN KEY (`id_dydaktyka`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `tagi_ankiety`
--
ALTER TABLE `tagi_ankiety`
  ADD CONSTRAINT `tagi_ankiety_ibfk_1` FOREIGN KEY (`id_ankiety`) REFERENCES `ankiety` (`id_ankiety`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `tagi_test`
--
ALTER TABLE `tagi_test`
  ADD CONSTRAINT `tagi_test_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `test` (`id_test`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`id_dydaktyka`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `tester_grupa`
--
ALTER TABLE `tester_grupa`
  ADD CONSTRAINT `tester_grupa_ibfk_1` FOREIGN KEY (`id_testera`) REFERENCES `users` (`id_users`) ON DELETE CASCADE,
  ADD CONSTRAINT `tester_grupa_ibfk_2` FOREIGN KEY (`id_grupy`) REFERENCES `grupy` (`id_grupy`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `wyniki`
--
ALTER TABLE `wyniki`
  ADD CONSTRAINT `wyniki_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `test` (`id_test`) ON DELETE CASCADE,
  ADD CONSTRAINT `wyniki_ibfk_2` FOREIGN KEY (`id_testera`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
