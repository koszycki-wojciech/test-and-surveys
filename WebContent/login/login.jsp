<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
<div class="external">
  <%@include file="../files/top.jsp" %>
  </div>
   <%@include file="../files/menu.jsp" %>
    <div class="content">
      <div class="content_top">
      <h2> Strona logowania</h2>
      </div>
      <div class="content_diff">
        <h4>Zaloguj sie...</h4>
<div>


<form method="post" action="j_security_check">
<table>
 <tr>
 <td>Login:</td>
 <td><input type="text" name="j_username" class="login_form" ></td>
 </tr>
 <tr>
 <td>Hasło:</td>
 <td><input type="password" name="j_password" class="login_form" ></td>
 </tr>
 </table>
 <div class="rejestracja"><input class="przycisk" type="submit" value="Zaloguj" ></div>
 </form>
 
</div>
 			</div>

      <div class="content_bottom"></div>
    </div>

   <%@include file="../files/foot.jsp" %>
</div>
</body>
</html>

