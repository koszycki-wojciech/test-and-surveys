<%@page import="pl.testy.service.RaportService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="style/etna.css" type="text/css" >


</head>
<body>
<div class="external">
  <%@include file="files/top.jsp" %>
  </div>
   <%@include file="files/menu.jsp" %>

     
    <div class="content">
      <div class="content_top">
      <h2><img src="images/globe_32x32.png" alt="globus"> Witaj na Testy i Ankiety online</h2>
      </div>
      <div class="content_diff">
        <h4>Dzięki nam możesz przeprowadzić egzamin z dowolnej tematyki za pośrednictwem przeglądarki internetowej.</h4>
       
        <ul>
 
 <li>Tworzenie testów za pomocą formularzy.</li>
 <li>Załączanie do pytań ilustracji i innych elementów medialnych.</li>
 <li>Pytania wyświetlane losowo</li>
 <li>Skuteczna kontrola tożsamości użytkowników.</li>
 <li>Możliwość tworzenia testów jedno- i wielokrotnego wyboru.</li>
 <li>Nadawania różnych wag pytaniom.</li>
 <li>Kontrola czasu trwania testu.</li>
 <li>Automatyczne ocenianie testów.</li>
 <li>Testy do wydruku</li>
        </ul>
        <p>Aby Uzyskać pełen dostęp do zasbów oraz funkcjonalnośći serwisu należy się zalogować. Rejestracja jest darmowa oraz zajmuje kilka minut </p><br>
        <br>
        <h4>Zapraszamy do korzystania !</h4>
        <p>zespół Testy i Ankiety online</p>
       
        
 			</div>

      <div class="content_bottom"></div>
    </div>

   <%@include file="files/foot.jsp" %>
</div>
</body>
</html>

