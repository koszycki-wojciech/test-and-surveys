<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet" href="<%=request.getContextPath()%>/style/etna.css" type="text/css" >
<script type="text/javascript" src="../jscript/loadjsRegisterForm.js"></script>
<script type="text/javascript" src="../jscript/validateRegisterForm.js"></script>
<script type="text/javascript" src="../jscript/jquery1.7.2.js"></script>
<script type="text/javascript" src="../jscript/jquery.ukrywaniepol.js"></script>
<script type="text/javascript">
				$(function() {

					$("#aktywator1").ukryjpola({docelowy : 'dodatkowo1', szybkoscpokaz : 'normal'});

				});
</script>

</head>
<body>
	<div class="external">
		<%@include file="top.jsp"%>
	</div>
	<%@include file="menu_rejestracja.jsp"%>

	<div class="content">
		<div class="content_top">
		<h2><img src="../images/key_2_32x32.png" alt="key">Rejestracja</h2>
		</div>
		<div class="content_diff">
			<h4>Wypełnij poniższy formularz</h4>
			<menu>
			<li>Po poprawnym wypełnieniu formularza zostanie wysłany email aktywacyjny</li>
			<li>Całkowity dostęp będzie udostępniony po aktywacji adresu email.</li>
			</menu>


			<form  name="registerform" id="registerform" onsubmit="onclickregisterbutton()"
				
				action="../Registerservlet" method="post"  >


				<div class="rejestracja">
				
			
			<label>Dydaktyk</label>
			<input type="radio"  name="radio_role" checked="checked" value="admin_role">
			
			<label>Student</label>
			<input id="aktywator1" type="radio" name="radio_role" value="user_role" >
			
			
			<div id="dodatkowo1" class="rejestracja">
			<table>
			<tr>
			<td><input type="text" name="logindydaktyka" value="Dydaktyk" ></td><td>Podaj login Dydaktyka</td>
			</tr>
			<tr>
			<td><input type="text" name="nazwaklasy" value="Klasa1" ></td><td>Jeżeli nie podano ci klasy pozostaw domyślnie</td>
			</tr>
			<tr>
			<td><input type="text" name="nazwagrupy" value="Grupa wszyscy" ></td><td>Jeżeli nie podano ci grupy pozostaw domyślnie</td>
			</tr>
			</table>
			</div>
			<p>Wszystkie pola są wymagane, hasło musi zawierać minimum 6 znaków</p>
			<table>

			<tr>
			<td><label>Imię</label></td>
			<td><input id="imie" type=text name="imie" >
			<div id="err_imie" class="error"> </div></td>
		</tr>
				<tr>
			<td><label>Nazwisko</label></td>
			<td><input id="nazwisko" type=text name="nazwisko" >
			<div id="err_nazwisko" class="error"> </div></td>
		</tr>
	<tr>
		<td><label>Podaj login</label></td>
		<td><input id="login" type=text name="login">
		<div id="err_login" class="error"> </div></td> 
	</tr>
		<tr>
			<td><label>Podaj swoje hasło</label></td>
			<td><input id="enterpassword" type=password name="password" >
			<div id="err_enterpassword" class="error"> </div></td>
		</tr>
		<tr>
			<td><label>Ponownie podaj swoje hasło</label></td>
			<td><input id="retypepassword" type=password name="retypepassword" >
			<div id="err_retypepassword" class="error"> </div></td>
		</tr>
		<tr>
			<td><label>Podaj adres email</label></td>	 
			<td><input id="email" type=text name="email" >
			<div id="err_email" class="error"> </div></td>
		</tr>

			</table>

					<input id="tosaccept" type="checkbox" name="akceptujeregulamin" >
					<label> Oświadczam że przeczytałem oraz akceptuje <a
						href="<%=request.getContextPath()%>/files/regulamin.jsp">Regulamin</a></label>
					<div id="err_tosaccept" class="error"></div>


					<div>
						<script type="text/javascript"
							src="http://api.recaptcha.net/challenge?k=6LfnstASAAAAAKdprsqhCrPMzrfWJDtFqf04fOi_">
							
						</script>
						<noscript>
							<iframe
								src="http://api.recaptcha.net/noscript?k=6LfnstASAAAAAKdprsqhCrPMzrfWJDtFqf04fOi_"
								height="300" width="500" frameborder="0"></iframe>
							
							<textarea name="recaptcha_challenge_field" id="recaptcha_challenge_field" rows="3" cols="40">
       </textarea>
							<input type="hidden" name="recaptcha_response_field" id="recaptcha_response_field"
								value="manual_challenge" >
						</noscript>
					</div>
					<div class="error" ><%
					
					
					if ("ON".equals(request.getAttribute("err_recaptcha"))) {
						out.println("Niewłaściwy kod captcha!!");
					}
					
					
					%></div>
					<p>
						<input  id="register" type="submit"   value="Wyślij" class="przycisk" >
				</div>


			</form>

		</div>

		<div class="content_bottom"></div>
	</div>

	<%@include file="foot.jsp"%>
	</div>
</body>
</html>

