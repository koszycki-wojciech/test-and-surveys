<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="<%=request.getContextPath()%>/style/etna.css" type="text/css" >

</head>
<body>
<div class="external">
  <%@include file="top.jsp" %>
  </div>
   <%@include file="menu.jsp" %>

     
    <div class="content">
      <div class="content_top">
      <h2><img src="../images/zoom_in_32x32.png" alt="zoom"> Jak to działa ?</h2>
      </div>
      <div class="content_diff">
        <h4>FaQ - Pytania i Odpowiedzi</h4>
     <p>Kategorie:</p>
 <h4>Rejestracja</h4>
	 <ol>
 <li>Rejestracja umożliwia pełny dostęp do aplikacji internetowej zarejestrowac można się na stronie www.testyiankiety.xxxx.pl/files/rejestracja.jsp wypełniając podany formularz.
 <li>Wszystkie pola formularza są wymagane. Po akceptacji formularza przez system wysłana zostanie na wcześniej podany adres email wiadomość w treści której będzie znajdował się odnośnik aktywacyjny konta który należy otworzyć.
 <li>Nie potwierdzenie adresu email do 24h od daty rejestracji skutkuje automatycznym usunięciem konta.
 <li>Rejestracja oraz korzystanie jest całkowicie darmowe.
 </ol>
 <h4>Interfejs</h4>
 <ol>
 <li>Aby korzystać z funkcji aplikacji należy poprawnie zalogować się do systemy wybierając typ użytkownika z menu bocznego po lewej stronie na stronie głównej www.testyiankiety.xxxx.pl
 <li> Poprawnie zalogowany użytkownik ma do dyspozycji Panel nawigacyjny który znajduje się pod menu bocznym z lewej strony
 <li> <b>Interfejs Dydaktyka</b>
 Kategorie:
Stwórz nowy
•	Stwórz Ankietę – formularz tworzenia Ankiety.
•	Stwórz Test – formularz tworzenia Testu.
Zarządzaj stworzonymi
•	Przegląd Ankiet – spis stworzonych ankiet oraz ich aktywacja lub dezaktywacja.
•	Przegląd Testów – spis stworzonych testów wraz z ich opisem, możliwość zmiany stanu testu.
•	Archiwum – Ankiety oraz testy przeniesione o stanie archiwizacji. 
Twoi użytkownicy
•	Dodaj Testera – formularz dodający użytkownika.
•	Przegląd Użytkowników – Lista użytkowników danego Dydaktyka z możliwością ich edycji.
Twoje klasy
•	Przegląd Klas – Lista aktualnych klas, po wskazaniu klasy wyświetlane są szczegóły klasy (lista jej grup), możliwość edycji nazw klas oraz grup.
•	Dodaj Klasę – formularz tworzenia klasy oraz grup do niej należących 
Statystyki
•	Statystyki testów – informacje o średnich dobrych odpowiedziach.
•	Statystyki ankiet – informacje o ilości kliknieć odpowiedz ankiety.
Inne
•	Przegląd Wyników – lista poszczególnych uczniów oraz ich wyniki z poszczególnych testów.
•	Udostępnij Materiały – formularz do wysyłania plików na serwer, pola wyboru której klasie dane pliki mają być udostępnione.
Twój profil
•	Edytuj Profil – formularz edycji profilu.
 <li> <b>Interfejs Testera</b>
•   Podejmij Wyzwanie - W podkategoriach użytkownik może wyświetlać aktualnie dostępne dla niego testy bądź ankiety.
•   Materiały dydaktyczne - użytkownik znajdzie pliki udostępnione przez swojego Dydaktyka.
•   Wyniki zawiera informację o wynikach rozwiązanych testów, jak również archiwum wyników gdzie będą znajdować się wyniki testów przeniesionych do archiwum.
•   Profil umieszczony jest interfejs który wyświetla w głównej zawartości, dany profil oraz pozwala na jego edycję.
</ol>
 <h4>Profil, konto oraz ustawienia  </h4>
 <ol>
 <li> W każdej chwili możesz zmienic ustawienia profilu korzystajac z swojego panelu nawigacyjnego.
 <li>login nie podlega zmianie.</li>
 </ol>
 <h4>Inne</h4>
 <ol>
<li> W przypadku jakichkolwiek pytan prosimy o kontak przy użyciu formularza kontaktowego umieszczonego na stronie 
        </ol>
        	</div>

      <div class="content_bottom"></div>
    </div>

   <%@include file="foot.jsp" %>
</div>
</body>
</html>

