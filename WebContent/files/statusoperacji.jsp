<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="<%=request.getContextPath()%>/style/etna.css" type="text/css" >

</head>
<body>
<div class="external">
  <%@include file="top.jsp" %>
  </div>
   <%@include file="menu.jsp" %>

     
    <div class="content">
      <div class="content_top">
      <h2>Status</h2>
      </div>
      <div class="content_diff">
        <h4>Komunikat o wyniku:</h4>
<p><%= request.getAttribute("message") %></p>
<hr>
<p><a href="<%= request.getContextPath() %><%= request.getAttribute("url") %>" >Powrót</a></p>
 			</div>

      <div class="content_bottom"></div>
    </div>

   <%@include file="foot.jsp" %>
</div>
</body>
</html>

