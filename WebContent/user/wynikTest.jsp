<%@page import="pl.testy.model.TestHeader"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon" href="../images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>

	<div class="content">
		<div class="content_top">
			<h2>
				Twój wynik
			</h2>
		</div>
		<div class="content_diff">
			<h4>WYNIK</h4>
<p>
			Gratulacje!<br>
			 Zdobyłeś:	<%=request.getAttribute("wynik")%>	punktów.
</p>
			
		</div>
</div>
		<%@include file="../files/foot.jsp"%>
	</div>
</body>
</html>

