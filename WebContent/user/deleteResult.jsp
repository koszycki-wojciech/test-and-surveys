<%@page import="pl.testy.db.manager.UserDbManager"%>
<%@page import="pl.testy.model.Wynik"%>
<%@page import="pl.testy.model.TestHeader"%>
<%@page import="pl.testy.db.manager.TestDbManager"%>
<%@page import="pl.testy.model.UserHeader"%>
<%@page import="java.util.List"%>
<%@page import="pl.testy.db.manager.TestDbManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" />

</head>
<body>
<div class="external">
  <%@include file="../files/top.jsp" %>
  </div>
   <%@include file="../files/menu.jsp" %>

     
    <div class="content">
      <div class="content_top">
      <h2>Archiwum</h2>
      </div>
      <div class="content_diff">
        <h4>Usuń</h4>
        <p>Wynik został usunięty z archiwum </p>
<%
TestDbManager tdm = new TestDbManager(); 
UserDbManager udm = new UserDbManager();
int iduser = udm.getIdByLogin(request.getUserPrincipal().getName());
int idwynik = Integer.parseInt(request.getParameter("id"));
tdm.deleteResultFromArchieve(iduser, idwynik);
%>
<p><a href="archieve.jsp" >Powrót</a></p>



  
    </div>

   <%@include file="../files/foot.jsp" %>
</div>
</body>
</html>

