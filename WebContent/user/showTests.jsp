<%@page import="pl.testy.model.TestHeader"%>
<%@page import="pl.testy.db.manager.TestDbManager"%>
<%@page import="pl.testy.model.UserHeader"%>
<%@page import="java.util.List"%>
<%@page import="pl.testy.db.manager.TestDbManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon" href="../images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>


	<div class="content">
		<div class="content_top">
			<h2>
				Testy do rozwiązania
			</h2>
		</div>
		<div class="content_diff">
			<h4>Lista dostępnych testów</h4>
			<%
				TestDbManager manager = new TestDbManager();
				String logintestera = request.getUserPrincipal().getName();
				List<TestHeader> testlist = manager.testlistuser(logintestera);
			%>

			<table>
				<tr>
					<td class="tableheader">Nazwa</td>
					<td class="tableheader">Opis</td>
					<td class="tableheader">Czas trwania</td>
					<td class="tableheader">Stan</td>
					<td class="tableheader">Akcja</td>
					<%
						for (TestHeader th : testlist) {
					%>
				
				<tr>
					<td><%=th.getNazwa()%></td>
					<td><%=th.getOpis()%></td>
					<td><%=th.getCzastrwania()%></td>
					<td><%=th.getStan()%></td>
					<td><a
						href="../user/viewtestservlet?id=<%=th.getIdtestu()%>">Wyświetl</a></td>
				</tr>
				<%
					}
				%>
			</table>
		</div>

		<div class="content_bottom"></div>
	</div>

	<%@include file="../files/foot.jsp"%>
	</div>
</body>
</html>

