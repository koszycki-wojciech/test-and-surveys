<%@page import="pl.testy.model.TestHeader"%>
<%@page import="pl.testy.db.manager.TestDbManager"%>
<%@page import="pl.testy.model.UserHeader"%>
<%@page import="java.util.List"%>
<%@page import="pl.testy.db.manager.TestDbManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
<div class="external">
  <%@include file="../files/top.jsp" %>
  </div>
   <%@include file="../files/menu.jsp" %>

     
    <div class="content">
      <div class="content_top">
      <h2>Przegląd Testów</h2>
      </div>
      <div class="content_diff">
        <h4>Lista Testów</h4>
<% TestDbManager manager = new TestDbManager(); 
String logindydaktyka = request.getUserPrincipal().getName();
List<TestHeader> testlist = manager.testlist(logindydaktyka);
%>

<table>
<tr><td>Nazwa</td><td>Opis</td><td>Czas trwania</td><td>Stan</td><td>Akcja</td>
<% for (TestHeader th: testlist ) { %>
<tr><td><%= th.getNazwa() %></td><td><%= th.getOpis() %></td><td><%= th.getCzastrwania() %> min</td><td><%= th.getStan() %></td> <td> <a href="showResult.jsp?id=<%=th.getIdtestu()  %>">Wyniki </a> <a href="../admin/activatetestservlet?id=<%=th.getIdtestu()  %>">Edytuj</a></td></tr>
<% } %>
</table>
<p>Legenda:</p>
			<menu>
			<li>AR Archiwum </li>
			<li>N Nieaktywny </li>
			<li>A Aktywny</li>
			</menu>
 			</div>

  
    </div>

   <%@include file="../files/foot.jsp" %>
</div>
</body>
</html>

