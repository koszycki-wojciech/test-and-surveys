<%@page import="pl.testy.model.UserHeader"%>
<%@page import="java.util.List"%>
<%@page import="pl.testy.db.manager.UserDbManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon" href="../images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>


	<div class="content">
		<div class="content_top">
			<h2>
				Przegląd Użytkowników
			</h2>
		</div>
		<div class="content_diff">
			<h4>Lista użytkowników</h4>
			<%
				UserDbManager manager = new UserDbManager();
				String logindydaktyka = request.getUserPrincipal().getName();
				List<UserHeader> userlist = manager.userlist(logindydaktyka);
			%>

			<table>
				<tr>
					<td class="tableheader">Imię</td>
					<td class="tableheader">Nazwisko</td>
					<td class="tableheader">E-mail</td>
					<td class="tableheader">Stan</td>
					<td class="tableheader">Klasa</td>
					<td class="tableheader">Grupa</td>
					<td class="tableheader">Akcja</td>
				</tr>
				<%
					for (UserHeader uh : userlist) {
				%>
				<tr>
					<td><%=uh.getImie()%></td>
					<td><%=uh.getNazwisko()%></td>
					<td><%=uh.getEmail()%></td>
					<td><%=uh.getStan()%></td>
					<td><%=uh.wypiszklase()%></td>
					<td><%=uh.wypiszgrupy()%></td>
					<td><a href="../edituserservlet?id=<%=uh.getId()%>">Edytuj</a><br><a href="../admin/deleteuserservlet?id=<%=uh.getId()%>">Usuń</a> </td>
				</tr>
				<%
					}
				%>
			</table>
		</div>

		<div class="content_bottom"></div>
	</div>

	<%@include file="../files/foot.jsp"%>
	</div>
</body>
</html>

