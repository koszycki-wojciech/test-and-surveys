<%@page import="pl.testy.model.ClassFull"%>
<%@page import="pl.testy.converter.StringConverter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon" href="../images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>

	<%
		ClassFull cf = (ClassFull) request.getAttribute("classedit");
	%>
	<!-- rzutowanie obiektu -->

	<div class="content">
		<div class="content_top">
			<h2>Edytuje Klasę</h2>
		</div>
		<div class="content_diff">

			Podaj jej nazwę, określ grupy. Każda klasa wymaga minimum jednej
			grupy np."wszyscy"
			<form method="post" name="myform" action="editclassservlet">

				<!-- String Converter text->null " " -->
				<%
					StringConverter sc = new StringConverter();

					Integer iloscgrup = 1;
					if (request.getAttribute("iloscgrup") != null) {
						iloscgrup = (Integer) request.getAttribute("iloscgrup");
					}
				%>
				<input type="hidden" value="<%=iloscgrup%>" name="ilosc"> <input
					type="hidden" name="f_old" value="<%=cf.getNazwaklasy()%>">
					<input type="hidden" value="<%= cf.getIdklasy()%>" name="idklasy">
				<div class="test_description">
					<table width="80px" cellspacing="2" cellpadding="0">
						<tr>
							<td nowrap>Nazwa Klasy:</td>
							<td width="110px"><input type="text" name="f_name"
								value="<%=cf.getNazwaklasy()%>" maxlength="125" size="60"></td>

						</tr>


					</table>
					<p>Uwaga ! usunięcie klasy spowoduje usunięcie wszystkich jej grup, oraz przynależność użytkowników do tych grup.</p>
				</div>
				<h4>Lista grup:</h4>
				<table>
					<tr>
						<td>Nazwa</td>
						<td>Akcja</td>
					</tr>
						<%
							for (int i = 0; i < iloscgrup; i++) {
						%>
						<div>
							<tr>
								<td><%=cf.getListagrup().get(i).getNazwagrupy()%></td>
								<td><a
									href="../admin/editgroupservlet?id=<%=cf.getListagrup().get(i).getIdgrupysql()%>">Edytuj</a>
									<input type="hidden" name="grupa[<%=i%>]"
									value="<%=cf.getListagrup().get(i).getNazwagrupy()%>"></td>
							
					</tr>



					
					<input type="hidden" name="del_qid" value="">
					<input type="hidden" name="add_qid" value="">
					<input type="hidden" name="del_answer_id" value="">
					<input type="hidden" name="action" value="save">
					<input type="hidden" name="test_id" value="">
					<input type="hidden" name="f_created_at" value="">
					<input type="hidden" name="f_status" value="0">
		</div>
		<%
			}
		%>
		</table>
		<div class="obok">
			 <input class="przycisk" type="submit" name="submit"
				value="dodaj grupe">
		</div>

		<hr>
		<div class="obok">
			<input class="przycisk" type="submit" name="submit"
				value="zapisz zmiany">
		</div>
		<div class="obok">
			<input class="przycisk" type="submit" name="submit"
				value="usun klase">
		</div>



		</form>





	</div>


	</div>
	<%@include file="../files/foot.jsp"%>
</body>
</html>

