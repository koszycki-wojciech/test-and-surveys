<%@page import="pl.testy.model.TestHeader"%>
<%@page import="pl.testy.db.manager.TestDbManager"%>
<%@page import="pl.testy.model.UserHeader"%>
<%@page import="java.util.List"%>
<%@page import="pl.testy.db.manager.TestDbManager"%>
<%@page import="pl.testy.model.SurveyHeader"%>
<%@page import="pl.testy.db.manager.SurveyDbManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
<div class="external">
  <%@include file="../files/top.jsp" %>
  </div>
   <%@include file="../files/menu.jsp" %>

     
    <div class="content">
      <div class="content_top">
      <h2>Archiwum</h2>
      </div>
      <div class="content_diff">
        <h4>Lista Testów</h4>
<% TestDbManager manager = new TestDbManager(); 
String logindydaktyka = request.getUserPrincipal().getName();
List<TestHeader> testlist = manager.testListArchieve(logindydaktyka);
%>

<table>
<tr><td>Nazwa</td><td>Opis</td><td>Czas trwania</td><td>Stan</td><td>Akcja</td>
<% for (TestHeader th: testlist ) { %>
<tr><td><%= th.getNazwa() %></td><td><%= th.getOpis() %></td><td><%= th.getCzastrwania() %></td><td><%= th.getStan() %></td><td><a href="../admin/activatetestservlet?id=<%= th.getIdtestu() %>">Edytuj Stan</a></td></tr>
<% } %>
</table>
        <h4>Lista Ankiet</h4>
        <% 
        SurveyDbManager smanager = new SurveyDbManager();
        List<SurveyHeader> surveylist = smanager.surveyListArchieve(logindydaktyka);
        
        %>
<table>
<tr><td>Nazwa</td><td>Stan</td><td>Akcja</td>
<% for (SurveyHeader sh: surveylist ) { %>
<tr><td><%= sh.getNazwa() %></td><td><%= sh.getStan() %></td><td><a href="../admin/activatesurveyservlet?id=<%= sh.getIdankiety() %>">Edytuj Stan</a></td></tr>
<% } %>
</table>
 			</div>
 			</div>

      <div class="content_bottom"></div>

   <%@include file="../files/foot.jsp" %>
</div>
</body>
</html>

