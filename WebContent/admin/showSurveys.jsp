<%@page import="pl.testy.model.SurveyHeader"%>
<%@page import="java.util.List"%>
<%@page import="pl.testy.db.manager.SurveyDbManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
<div class="external">
  <%@include file="../files/top.jsp" %>
  </div>
   <%@include file="../files/menu.jsp" %>

     
    <div class="content">
      <div class="content_top">
      <h2>Przegląd Ankiet</h2>
      </div>
      <div class="content_diff">
        <h4>Aktualne Ankiety</h4>
        <p>Jeżeli ankiety nie ma na liście sprawdź czy nie została przeniesiona do archiwum</p>
        <% 
        SurveyDbManager manager = new SurveyDbManager();
        String logindydaktyka = request.getUserPrincipal().getName();
        List<SurveyHeader> surveylist = manager.surveyList(logindydaktyka);
        
        %>
<table>
<tr><td>Nazwa</td><td>Stan</td><td>Akcja</td>
<% for (SurveyHeader sh: surveylist ) { %>
<tr><td><%= sh.getNazwa() %></td><td><%= sh.getStan() %></td><td> <a href="showSurveyResult.jsp?id=<%=sh.getIdankiety()  %>">Wyniki </a><br><a href="../admin/activatesurveyservlet?id=<%= sh.getIdankiety() %>">Aktywuj</a></td></tr>
<% } %>
</table>
<p>Legenda:</p>
			<menu>
			<li>AR Archiwum </li>
			<li>N Nieaktywny </li>
			<li>A Aktywny</li>
			</menu>
 			</div>

     
    </div>

   <%@include file="../files/foot.jsp" %>
</div>
</body>
</html>

