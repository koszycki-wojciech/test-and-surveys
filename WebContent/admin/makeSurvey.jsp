<%@page import="pl.testy.converter.StringConverter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>


	<div class="content">
		<div class="content_top" >
		<h2>
			 Nowa Ankieta
		</h2>
		</div>
		<div class="content_diff">
<menu>
			<li>Podaj jej nazwę, tagi, pytania i odpowiedzi.</li>
			<li>Dodając zdjęcia wklej bezpośredni odnośnik do zdjęcia</li>
			<li>Dodając filmy wklej kod typu "umiesć na stronie"</li>		
</menu>
			
			<form method="post" name="myform" action="makesurveyservlet">

				<!-- String Converter text->null " " -->
				<%
					StringConverter sc = new StringConverter();

					Integer iloscpytan = 1;
					if (request.getAttribute("iloscpytan") != null) {
						iloscpytan = (Integer) request.getAttribute("iloscpytan");
					}
				%>
				<input type="hidden" value="<%=iloscpytan%>" name="ilosc">


				<div class="test_description">
					<table width="80px" cellspacing="2" cellpadding="0">
						<tr>
							<td nowrap>Nazwa Ankiety:</td>
							<td width="110px"><input type="text" name="f_name"
								value="<%=sc.convertText(request.getParameter("f_name"))%>"
								maxlength="125" size="60"></td>
						</tr>

						<tr>
							<td>Tagi:</td>
							<td><input type="text" name="f_tags"
								value="<%=sc.convertText(request.getParameter("f_tags"))%>"
								size="60"><br> <span class="info"><span
									class="green">Tagi</span> to słowa, najlepiej opisujące Twoją
									ankietę<br>np: humor przyjaźń</span></td>
						</tr>
					</table>
				</div>
				<%
					for (int i = 0; i < iloscpytan; i++) {
				%>
				<div class="pytanie">
					<input type="hidden" name="f_privacy" value="0"> <a
						name="pyt1"></a>
					<h2>
						Pytanie
						<%=i + 1%></h2>
<p>
					<%
						Integer iloscodpowiedzi = 1;
							if (request.getAttribute("iloscodpowiedzi" + i) != null) {
								iloscodpowiedzi = (Integer) request
										.getAttribute("iloscodpowiedzi" + i);
							}
					%>
</p>

					<input type="hidden" value="<%=iloscodpowiedzi%>"
						name="iloscodpowiedzi<%=i%>"> <input type="text"
						name="f_question[<%=i%>]"
						value="<%=sc.convertText(request.getParameter("f_question[" + i
						+ "]"))%>"
						maxlength="125" size="60"><input type="hidden"
						name="f_qid[1]" value=""> 
						<div>Media<br>
						Foto:
						<input type="text" name="foto[<%=i%>]" value="<%=sc.convertText(request.getParameter("foto[" + i+ "]"))%>" size="10">
						Video:
						<input type="text" name="video[<%=i%>]" value="<%=sc.convertText(request.getParameter("video[" + i+ "]"))%>" size="10">
						</div>
						<br> 
						
						<div>Odpowiedzi:</div>
					<%
						for (int j = 0; j < iloscodpowiedzi; j++) {
					%>
					<div class="obok" >
					<input type="text" name="f_answer[<%=i%>][<%=j%>]"
						value="<%=sc.convertText(request.getParameter("f_answer["
							+ i + "]["+ j +"]"))%>"
						maxlength="125" size="60"></div>
						

					<%
						}
					%>
					<div class="obok">
					<input class="przycisk" type="submit" name="submit_add_anserw<%=i%>"
						value="dodaj odpowiedz"><input class="przycisk" type="submit" name="submit_delete_anserw<%=i%>"
						value="usuń odpowiedź"></div>
					<br>
					
					 <input type="hidden" name="del_qid" value=""> <input
						type="hidden" name="add_qid" value=""> <input
						type="hidden" name="del_answer_id" value=""> <input
						type="hidden" name="action" value="save"> <input
						type="hidden" name="test_id" value=""> <input
						type="hidden" name="f_created_at" value=""> <input
						type="hidden" name="f_status" value="0">
				</div>
				<%
					}
				%>
						<div>
						<input class="przycisk" type="submit" name="submit" value="dodaj pytanie"> 
						<input class="przycisk" type="submit"name="submit" value="usun pytanie">
						</div>
						<hr>
				<div class="obok"> <input class="przycisk" type="submit" name="submit"
					value="zapisz zmiany"></div> 



			</form>




			
		</div>

		
	</div>
	<%@include file="../files/foot.jsp"%>
	</div>
</body>
</html>

