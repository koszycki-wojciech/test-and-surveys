<%@page import="pl.testy.model.UserFull"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon"
	href="<%=request.getContextPath()%>/images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/style/etna.css" type="text/css" >

<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/loadjsDeleteAccount.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/deleteAccountForm.js"></script>

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>
	<%
		UserFull uf = (UserFull) request.getAttribute("useredit");
	%>
	<!--  rzutowanie obiektu (UserFull) -->

	<div class="content">
		<div class="content_top">
			<h2>Usuń Konto</h2>
		</div>
		<div class="content_diff">
			<h4>Konto zostanie usunięte zmiany są nieodwracalne</h4>
<p>INFO: Wraz z kontem zostaną usunięci wszyscy powiązani użytkownicy.</p>
			<form id="registerform"
				action="deleteaccountservlet" method="post">


				<table>
					<tr>
						<td><label>Imię</label></td>
						<td><%=uf.getImie()%>
							<div id="err_imie" class="error"></div></td>
					</tr>
					<tr>
						<td><label>Nazwisko</label></td>
						<td><%=uf.getNazwisko()%>
							<div id="err_nazwisko" class="error"></div></td>
					</tr>
					<tr>
						<td><label>Login</label></td>
						<td><%=uf.getLogin()%>
							<div id="err_login" class="error"></div></td>
					</tr>

					<tr>
						<td><label>Email</label></td>
						<td><%=uf.getEmail()%>
							<div id="err_email" class="error"></div></td>
					</tr>

				</table>
				<input type="hidden" name="iduser" value="<%=uf.getId()%>">


				<div>


					<p>
						<input class="przycisk" id="register" type="submit"
							name="register" value="Usuń"></p>
				</div>

			</form>


		</div>

		<div class="content_bottom"></div>
	</div>

	<%@include file="../files/foot.jsp"%>
	</div>
</body>
</html>

