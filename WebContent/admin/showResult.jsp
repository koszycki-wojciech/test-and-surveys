<%@page import="pl.testy.model.Wynik"%>
<%@page import="pl.testy.model.TestHeader"%>
<%@page import="pl.testy.db.manager.TestDbManager"%>
<%@page import="pl.testy.model.UserHeader"%>
<%@page import="java.util.List"%>
<%@page import="pl.testy.db.manager.TestDbManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
<div class="external">
  <%@include file="../files/top.jsp" %>
  </div>
   <%@include file="../files/menu.jsp" %>

     
    <div class="content">
      <div class="content_top">
      <h2>Przegląd Testów</h2>
      </div>
      <div class="content_diff">
        <h4>Lista Wyników</h4>
<% TestDbManager manager = new TestDbManager(); 
String logindydaktyka = request.getUserPrincipal().getName();
int idtestu = Integer.parseInt(request.getParameter("id"));
List<Wynik> testlist = manager.wynikList(idtestu);
%>

<table>
<tr><td>Imie</td><td>Nazwisko</td><td>Wynik</td><td>Data start</td>
<% for (Wynik th: testlist ) { %>
<tr><td><%= th.getImie() %></td><td><%= th.getNazwisko() %></td><td><%= th.getZdobytywynik() %></td><td><%= th.getDatastart() %></td></tr>
<% } %>
</table>

<% if(testlist.size() > 0){ %>
	<p><a href="../WynikiExport?idTestu=<%=idtestu %>">Eksportuj do PDF</a></p>
<%} else { %>
	<p> Eksportuj do PDF (nieaktywny z powodu braku danych).</p>
<%} %>

 			</div>

  
    </div>

   <%@include file="../files/foot.jsp" %>
</div>
</body>
</html>

