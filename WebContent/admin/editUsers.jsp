<%@page import="pl.testy.model.UserFull"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon"
	href="<%=request.getContextPath()%>/images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/style/etna.css" type="text/css" >
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/jquery1.7.2.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/jquery.ukrywaniepol.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/loadjsRegisterForm.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/validateRegisterForm.js"></script>

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>

	<%
		UserFull uf = (UserFull) request.getAttribute("useredit");
	%>
	<!--  rzutowanie obiektu (UserFull) -->


	<div class="content">
		<div class="content_top">
			<h2>Edytuj Użytkownika</h2>
		</div>
		<div class="content_diff">
			<h4>Wypełnij poniższy formularz</h4>

			<form id="formularzrejestracji"
				onsubmit="return onclickregisterbutton()"
				action="edituserservlet" method="post">

 <input type="hidden" name="logindydaktyka"
					value="<%=request.getUserPrincipal().getName()%>" >
				<table>
					<tr>
						<td><label>Imię</label></td>
						<td><input id="imie" type=text name="imie"
							value="<%=uf.getImie()%>" >
							<div id="err_imie" class="error"></div></td>
					</tr>
					<tr>
						<td><label>Nazwisko</label></td>
						<td><input id="nazwisko" type=text name="nazwisko"
							value="<%=uf.getNazwisko()%>" >
							<div id="err_nazwisko" class="error"></div></td>
					</tr>
					<tr>
						<td><label>Podaj login</label></td>
						<td><input id="login" type=text name="login"
							value="<%=uf.getLogin()%>" >
							<div id="err_login" class="error"></div></td>
					</tr>

					<tr>
						<td><label>Podaj adres email</label></td>
						<td><input id="email" type=text name="email"
							value="<%=uf.getEmail()%>" >
							<div id="err_email" class="error"></div></td>
					</tr>
					<tr>
						<td><label>Należy do klasy</label></td>
						<td><input id="klasa" type=text name="klasa"
							value="<%=uf.getKlasa()%>" >
							<div id="err_class" class="error"></div></td>
					</tr>
					<tr>
						<td><label>Należy do grupy</label></td>
						<td><input id="grupa" type=text name="grupa"
							value="<%=uf.getGrupa()%>" >
							<div id="err_group" class="error"></div></td>
					</tr>
				</table>
				<input type="hidden" name="iduser" value="<%=uf.getId()%>">


				<div>


					<p>
						<input class="przycisk" id="register" type="submit"
							name="register" value="Wyślij"></p>
				</div>

			</form>


		</div>

		<div class="content_bottom"></div>
	</div>

	<%@include file="../files/foot.jsp"%>
	</div>
</body>
</html>

