<%@page import="pl.testy.model.SurveyHeader"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/style/etna.css" type="text/css" >

</head>
<body>
	<div class="external">
		<%@include file="/files/top.jsp"%>
	</div>
	<%@include file="/files/menu.jsp"%>

	<%
	
	SurveyHeader th = (SurveyHeader) request.getAttribute("testedit");
	%>
	<!--  rzutowanie obiektu (UserFull) -->


	<div class="content">
		<div class="content_top">
			<h2>Edytuj Ankietę</h2>
		</div>

			<div class="content_diff">
				<h4>Wypełnij poniższy formularz</h4>

				<form id="formularzrejestracji"
					onsubmit="return onclickregisterbutton()"
					action="activatesurveyservlet" method="post">


					<table>
						<tr>
							<td><label>Nazwa Ankiety</label></td>
							<td><input id="imie" type=text name="nazwa"
								value="<%=th.getNazwa()%>" />
								<div id="err_imie" class="error"></div></td>
						</tr>
						<tr>
							<td><label>Wybierz stan. Aktualny Stan (<%=th.getStan()%>)</label></td>
							<td><select name="stan" >
							<option><%=th.getStan()%></option>
							<option>N</option>
							<option>A</option>
							<option>AR</option>
							</select>    <!--   <input id="klasa" type=text name="stan"
								value="" />
								<div id="err_class" class="error"></div>--></td>
						</tr>

					</table>
					<input type="hidden" name="idtestu" value="<%=th.getIdankiety()%>">


					<div>


						<p>
							<input class="przycisk" id="register" type="submit"
								name="register" value="Wyślij">
					</div>

				</form>
<p>Legenda:</p>
			<menu>
			<li>N Nieaktywny </li>
			<li>A Aktywny</li>
			</menu>

			</div>

			<div class="content_bottom"></div>
		</div>

		<%@include file="/files/foot.jsp"%>
	</div>
</body>
</html>