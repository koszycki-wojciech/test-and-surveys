package pl.testy.converter;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/** Klasa do obsługi szyfrowania metodą md5
 * @author wojtek
 *
 */
public class MD5Genegate {
/** Metoda konwertująca ciąg znaków
 * 
 * @param pass parametr przyjmujący hasło
 * @return zwraca ciąg znaków haszowanych metodą md5
 */
	public String generate(String pass) {
		try {
			MessageDigest m = MessageDigest.getInstance("MD5");

			m.update(pass.getBytes(), 0, pass.length());
			return new BigInteger(1, m.digest()).toString(16);

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}
