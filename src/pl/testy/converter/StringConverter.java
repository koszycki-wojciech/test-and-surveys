package pl.testy.converter;


/**
 * Klasa obsługująca konwersje ciągów znaków
 * @author wojtek
 *
 */
public class StringConverter {

	/**
	 * Metoda zamieniająca wartości null na pusty string
	 * @param text tekst w parametrze
	 * @return tekst z parametru
	 */
public String convertText(String text){
	
	if(text == null){
		return "";
	}
	return text;
}
/**
 * Metoda zamieniająca wartości null na ciąg znaków przychodzących w parametrze
 * @param text
 * @param defaulttext text tekst w parametrze
 * @return tekst z parametru
 */
public String convertText(String text, String defaulttext ){
	
	if(text == null){
		return defaulttext;
	}
	return text;
}
	
	
	
}
