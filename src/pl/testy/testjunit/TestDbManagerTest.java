package pl.testy.testjunit;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pl.testy.db.manager.Dbmanager;
import pl.testy.db.manager.SurveyDbManager;
import pl.testy.db.manager.TestDbManager;
import pl.testy.db.manager.UserDbManager;
import pl.testy.model.Ankieta;
import pl.testy.model.Odpowiedz;
import pl.testy.model.Pytanie;
import pl.testy.model.SurveyHeader;
import pl.testy.model.TestHeader;

public class TestDbManagerTest {

	private SurveyDbManager sdm;
	private UserDbManager udm;
	private TestDbManager tdm;

	@Before
	public void prepare() {
		Dbmanager.DATA_BASE = "jdbc:mysql://localhost/testdbtest?characterEncoding=UTF-8";
		sdm = new SurveyDbManager();
		udm = new UserDbManager();
		tdm = new TestDbManager();
		udm.deleteAllUsers();
	}

	@Test
	public void testSaveDydaktyk() {

		String imie = "testimie";
		String nazwisko = "testnazwisko";
		String login = "testlogin";
		String password = "testpassword";
		String email = "testemail@testemail";
		String role = "admin";
		
		udm.saveDydaktyk(imie, nazwisko, login, password, email, role);
		
	}

	@Test
	public void testSaveUser() {

		String imie = "testimie";
		String nazwisko = "testnazwisko";
		String login = "testuserlogin";
		String password = "testpassword";
		String email = "testemail@testemail";
		String role = "user";
		String logindydaktyka = "testlogin";
		String nazwaklasy = "Klasa1";
		String nazwagrupy = "Grupa wszyscy";

		udm.saveUser(imie, nazwisko, login, password, email, role, logindydaktyka, nazwaklasy, nazwagrupy);
	}


	@Test
	public void testSaveSurvey() {

		udm.saveDydaktyk("testimie", "testnazwisko", "testlogin",
				"testpassword", "email@email.pl", "admin");

		String tytul = "tytuł ankiety";
		String login = "testlogin";
		List<Pytanie> listapytan = new ArrayList<Pytanie>();

		Pytanie p = new Pytanie();
		Odpowiedz o = new Odpowiedz();
		p.setTresc("testowa tresc");
		p.setListaodpowiedz(new ArrayList<Odpowiedz>());
		o.setTresc("testowa tresc odpowiedzi");
		p.getListaodpowiedz().add(o);
		listapytan.add(p);

		sdm.saveSurvey(tytul, login, listapytan);

		List<SurveyHeader> listankiet = sdm.surveyList(login);
		Assert.assertEquals(1, listankiet.size());
		int idankiety = listankiet.get(0).getIdankiety();
		Ankieta a = sdm.surveyResult(idankiety);
		Assert.assertEquals(tytul, a.getNazwa());
		Assert.assertEquals(1, a.getListapytan().size());
		Assert.assertEquals("testowa tresc", a.getListapytan().get(0)
				.getTresc());
		Assert.assertEquals(1, a.getListapytan().get(0).getListaodpowiedz()
				.size());
		Assert.assertEquals("testowa tresc odpowiedzi", a.getListapytan()
				.get(0).getListaodpowiedz().get(0).getTresc());
		Assert.assertEquals(0, a.getListapytan().get(0).getListaodpowiedz()
				.get(0).getIlosc_klikniec());
		Assert.assertEquals("N", a.getStan());

	}

	@Test
	public void testSaveTest() {



		String tytul = "tytul testu";
		String login = "testlogin";
		String opis = "testowy opis";
		int punkty = 10;
		int czastrwania = 10;
		List<Pytanie> listapytan = new ArrayList<Pytanie>();

		Pytanie p = new Pytanie();
		Odpowiedz o = new Odpowiedz();
		p.setTresc("testowa tresc");
		p.setListaodpowiedz(new ArrayList<Odpowiedz>());
		o.setTresc("testowa tresc odpowiedzi");
		p.getListaodpowiedz().add(o);
		listapytan.add(p);

		tdm.saveTest(tytul, opis, login, listapytan, punkty, czastrwania);

		List<TestHeader> listatestow = tdm.testlist(login);

		Assert.assertEquals(1, listatestow.size());
		int idtestu = listatestow.get(0).getIdtestu();
		;

		TestHeader th = tdm.showTest(idtestu);

		Assert.assertEquals(10, th.getCzastrwania());
		Assert.assertEquals("tytuł testu", th.getNazwa());
		Assert.assertEquals(punkty, th.getMaxpunkty());
		Assert.assertEquals("N", th.getStan());

	}
	@Test
	public void testSurveyFinish(){
		udm.saveDydaktyk("testimie", "testnazwisko", "testlogin",
				"testpassword", "email@email.pl", "admin");

		String tytul = "tytuł ankiety";
		String login = "testlogin";
		List<Pytanie> listapytan = new ArrayList<Pytanie>();

		Pytanie p = new Pytanie();
		Odpowiedz o = new Odpowiedz();
		p.setTresc("testowa tresc");
		p.setListaodpowiedz(new ArrayList<Odpowiedz>());
		o.setTresc("testowa tresc odpowiedzi");
		p.getListaodpowiedz().add(o);
		listapytan.add(p);

		sdm.saveSurvey(tytul, login, listapytan);
	}

	@Test
	public void testStartTest() {

		int idtestu = 1;
		int idtestera = 1;

		tdm.starttest(idtestu, idtestera);

	}

	@Test
	public void testUpdateWynik() {

		String login = "login";
		int testId = 1;
		int wynikPunktow = 1;

		tdm.updateWynik(login, testId, wynikPunktow);

	}
	@After
	public void cleanAfter() {
		udm.deleteAllUsers();
	}

}
