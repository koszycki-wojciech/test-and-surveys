package pl.testy.testjunit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pl.testy.db.manager.Dbmanager;
import pl.testy.walidator.Walidator;

public class TestWalidator {

	private Walidator w;
	@Before
	public void prepare() {
		Dbmanager.DATA_BASE = "jdbc:mysql://localhost/testdbtest?characterEncoding=UTF-8";
	w = new Walidator();
	
	}
	
	//walidacja pustych pól
	@Test
	public void testValidateEmptyString() {

		String param = "";
		
	 boolean i = w.validateEmptyString(param);
		boolean j = true;
		Assert.assertEquals(j, i);
	}
	@Test
	public void testValidateNull() {

		String param = "";
		boolean i = w.validateNull(param);
		boolean j = false;
		Assert.assertEquals(j, i);
	}
//walidacja dlugosci hasla
	@Test
	public void testValidateLenghtString() {
		String param = "pa";
		boolean i = w.validateLenghtString(param);
		boolean j = true;
		Assert.assertEquals(j, i);
	}
//walidacja formatu adresu email
	@Test
	public void testValidateEmail() {
		String param = "parametrparametr.pl";
		boolean i = w.validateEmail(param);
		boolean j = true;
		Assert.assertEquals(j, i);
	}
// walidacja poprawnie wpisanych haseł
	@Test
	public void testValidatePassword() {
		String param1 = "parametra";
		String param2 = "parametr";
		boolean i = w.validatePassword(param1, param2);
		boolean j = true;
		Assert.assertEquals(j, i);
	}
	
	
}
