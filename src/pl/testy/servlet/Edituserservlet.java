package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.UserDbManager;
import pl.testy.model.UserFull;
import pl.testy.walidator.Walidator;

/**
 * Servlet implementation class Editservlet
 */
@WebServlet("/edituserservlet")
public class Edituserservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Edituserservlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		UserDbManager mgm = new UserDbManager(); // tworzy obiekt klasy
													// UserDbManager
		UserFull uf = mgm.getUserById(Integer.parseInt(request
				.getParameter("id"))); // wywoluje metode wszystko co przychodzi
										// w request jest w formie string->
										// parse

		request.setAttribute("useredit", uf);
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/admin/editUsers.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String message = "";
		try {
			Walidator walidator = new Walidator();

			// Pobranie danych formularza rejestracji
			String imie = request.getParameter("imie");
			String nazwisko = request.getParameter("nazwisko");
			String login = request.getParameter("login");
			String email = request.getParameter("email");
			String klasa = request.getParameter("klasa");
			String grupa = request.getParameter("grupa");
			String logindydaktyk = request.getUserPrincipal().getName();
			int iddydaktyk;
			int iduser = (Integer.parseInt(request.getParameter("iduser")));
			
			UserDbManager walidacjalogin = new UserDbManager();
			walidacjalogin.checkUser(login);

			boolean loginwalidator = walidacjalogin.checkUser(login);
			boolean imiewalidator = walidator.validateEmptyString(imie);
			boolean nazwiskowalidator = walidator.validateEmptyString(nazwisko);
			boolean emailwalidator = walidator.validateEmail(email);

			UserDbManager manager = new UserDbManager();
			iddydaktyk = manager.getIdByLogin(logindydaktyk);
			
			if (loginwalidator) {

				message += "  Błąd Login istnieje w bazie";
			}
			if (imiewalidator && nazwiskowalidator) {

				message += " Błąd Pola wymagane";
			}
			if (emailwalidator) {

				message += "  Błąd Niepoprawny format email";
			}

			manager.updateUser(iduser,iddydaktyk, imie, nazwisko, email, login, klasa, grupa);
			message = "Dane zapisane..";
		} catch (Exception e) { // przechwycenie wyjatkow
			message = "Wyjątek Krytyczny"; // TODO: handle exception
		}

		finally { // ten kod wykona sie nawet jezeli bedzie wyjatek
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response); // TODO: handle exception
		}
	}
}
