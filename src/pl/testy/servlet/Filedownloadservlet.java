package pl.testy.servlet;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.UserDbManager;
import pl.testy.model.Plik;

/**
 * Servlet implementation class Filedownloadservlet
 */
@WebServlet("/Filedownloadservlet")
public class Filedownloadservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Filedownloadservlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		int idplik = Integer.parseInt(request.getParameter("id"));
	

		UserDbManager udm = new UserDbManager();
		Plik plikdb = udm.getFile(idplik);

		try {
			
		    
		    ServletOutputStream out = response.getOutputStream();

		    response.setContentType( plikdb.getTyp() );
		    response.setHeader("Content-Disposition","attachment; filename=\""+plikdb.getNazwa()+"\"");
		   

		      InputStream in = plikdb.getPlikblob().getBinaryStream();
		      int length = (int) plikdb.getPlikblob().length();

		      int bufferSize = (int)plikdb.getPlikblob().length();
		      byte[] buffer = new byte[bufferSize];

		      while ((length = in.read(buffer)) != -1) {
		        out.write(buffer, 0, length);
		      }

		      in.close();
		      out.flush();
		} catch (Exception e) {
		e.printStackTrace();	// TODO: handle exception
		}
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
