package pl.testy.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.ClassDbManager;
import pl.testy.model.Grupa;

/**
 * Servlet implementation class maketestservlet
 */
@WebServlet("/admin/addclassservlet")
public class Addclassservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	// metoda do ustawiania atrybutow
	private void setAllAttribute(HttpServletRequest request) {

		  Integer iloscgrup = Integer.parseInt(request.getParameter("ilosc"));

		  request.setAttribute("iloscgrup", iloscgrup);

		
	}
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		setAllAttribute(request);
		String message = "";
		String submit = request.getParameter("submit");
		Integer iloscgrup = Integer.parseInt(request.getParameter("ilosc"));
		String url = "/admin/addClass.jsp";
		
		if ("dodaj grupe".equals(submit)) {

			request.setAttribute("iloscgrup", iloscgrup + 1);
		
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/admin/addClass.jsp");
			dispatcher.forward(request, response);

		}
		if ("usun".equals(submit)) {

			request.setAttribute("iloscgrup", iloscgrup - 1);
		
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/admin/addClass.jsp");
			dispatcher.forward(request, response);

		}

		if ("zapisz zmiany".equals(submit)){
			
			
			String nazwa = request.getParameter("f_name");
			
			String logindydaktyka = request.getUserPrincipal().getName();
			
			ClassDbManager cdm = new ClassDbManager();
			
			boolean klasajest = cdm.checkClass(nazwa, logindydaktyka);
			
			if(iloscgrup <= 0){
			
				message = "Klasa wymaga minimum jednej grupy np. Grupa Wszyscy ";
				request.setAttribute("message", message);
				request.setAttribute("url", url);
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/files/statusoperacji.jsp");
				dispatcher.forward(request, response);
			} else {
				
			
			
			if(!klasajest){
				
				List<Grupa> listagrup = new ArrayList<Grupa>();
				
				for ( int i = 0; i < iloscgrup; i++){
					Grupa g = new Grupa();
					g.setNazwagrupy(request.getParameter("f_group["+i+"]"));
					listagrup.add(g);
					}
					
					
				
				ClassDbManager classDbManager = new ClassDbManager();
				classDbManager.saveClass(nazwa, logindydaktyka, listagrup);
				
				message = "Dane zapisane klasa o nazwie <b>" +nazwa+ "</b> posiadającej <b>"+iloscgrup+"</b> grup zapisana!";
				request.setAttribute("message", message);
				request.setAttribute("url", url);
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/files/statusoperacji.jsp");
				dispatcher.forward(request, response);
			
			} else {
				message = "Juz posiadasz klasę o nazwie <b>" +nazwa+ "</b> !";
				request.setAttribute("message", message);
				request.setAttribute("url", url);
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/files/statusoperacji.jsp");
				dispatcher.forward(request, response);
			}
			}
			
		}
	  }
	}
	


