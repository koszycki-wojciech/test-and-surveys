package pl.testy.servlet;


import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;


@WebServlet("/Sendmailservlet")
public class Sendmailservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

 private static final String HOST = "smtp.gmail.com";
 private static final int PORT = 465;
 // Adres email osby która wysyła maila
 private static final String FROM = "testy.ankiety@gmail.com";
 // Hasło do konta osoby która wysyła maila
 private static final String PASSWORD = "testyiankiety";
 // Adres email osoby do której wysyłany jest mail
 private static final String TO = "koszycki.wojciech@gmail.com";



 public static void main(String[] args) {
  try {
   new Sendmailservlet().send(null, null);
  } catch (MessagingException e) {
   e.printStackTrace();
  }
 }

 public void send(String titlesend, String contentsend) throws MessagingException {

	  Properties props = new Properties();
	  props.put("mail.transport.protocol", "smtps");
	  props.put("mail.smtps.auth", "true");

	  // Inicjalizacja sesji
	  Session mailSession = Session.getDefaultInstance(props);

	  // ustawienie debagowania,
	  
	  mailSession.setDebug(true);

	  // Tworzenie wiadomości email
	  MimeMessage message = new MimeMessage(mailSession);
	  message.setSubject(titlesend);
	  message.setContent(contentsend, "text/plain; charset=ISO-8859-2");
	  message.addRecipient(Message.RecipientType.TO, new InternetAddress(TO));

	  Transport transport = mailSession.getTransport();
	  transport.connect(HOST, PORT, FROM, PASSWORD);

	  // wysłanie wiadomości
	  transport.sendMessage(message, message
	    .getRecipients(Message.RecipientType.TO));
	  transport.close();
	 }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		String url = "/index.jsp";
		String message = "";
		String titlesend = request.getParameter("title");
		String authorname = request.getParameter("authorname");
		String email = request.getParameter("email");
		String contentsend = request.getParameter("text");
		contentsend = contentsend +" -#-   NADAWCA  "+authorname + " -#-  ADRES ZWROTNY  " + email;
		  
		String challengeReCaptcha = request.getParameter("recaptcha_challenge_field");
		String responseReCaptcha = request.getParameter("recaptcha_response_field");
		String remoteAddr = request.getRemoteAddr(); //Address użytkownika
		      
		   
		   ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
		   
		   //ustawiamy nasz klucz prywatny
		      reCaptcha.setPrivateKey("6LfnstASAAAAALjbJGdRHg2M1NqFynpS-JcbEn6X");
		      
		      //sprawdzamy czy użytkownik wpisał pobrany wyraz w recaptcha
		      ReCaptchaResponse reCaptchaResponse =
		          reCaptcha.checkAnswer(remoteAddr, challengeReCaptcha, responseReCaptcha);
		      
		      if (!reCaptchaResponse.isValid()) {
		       response.sendRedirect("/index.jsp");
		      } 
		
		
		
		try {
			send(titlesend, contentsend);
			message += "Wiadomość wysłana, odpowiemy do 2 dni roboczych";
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message += e; 
		} finally {
			request.setAttribute("url", url);
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
				    .getRequestDispatcher("/files/statusoperacji.jsp");
				  dispatcher.forward(request, response);
		}
		
		


	}

}

