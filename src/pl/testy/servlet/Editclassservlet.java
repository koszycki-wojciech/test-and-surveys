package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.ClassDbManager;
import pl.testy.model.ClassFull;
import pl.testy.walidator.AuthCheck;

/**
 * Servlet implementation class Editclassservlet
 */
@WebServlet("/admin/editclassservlet")
public class Editclassservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Editclassservlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String message = "";

		AuthCheck acheck = new AuthCheck();
		if (!acheck.checkClassPermisson(request.getUserPrincipal().getName(),
				Integer.parseInt(request.getParameter("id")))) {

			message = "Brak dostępu !!!";
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
			return;
		}

		ClassDbManager mgm = new ClassDbManager();

		ClassFull cf = mgm.getClassById(Integer.parseInt(request
				.getParameter("id")));

		request.setAttribute("classedit", cf);
		request.setAttribute("iloscgrup", cf.getListagrup().size());
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/admin/editClass.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String message = "";
		String submit = request.getParameter("submit");
		int idklasy = Integer.parseInt(request.getParameter("idklasy"));
		ClassDbManager cdm = new ClassDbManager();
		String nowanazwa = request.getParameter("f_name");
		String nazwa = request.getParameter("f_old");
		String logindydaktyka = request.getUserPrincipal().getName();
		boolean klasajest = cdm.checkClass(nowanazwa, logindydaktyka);
		String url = "/admin/editclassservlet?id="+idklasy+"";

		try {

			if ("usun klase".equals(submit)) {
				
				cdm.deleteClass(idklasy);
				message = "Klasa o nazwie " + nazwa + " została usunięta";
				url = "/admin/showClasses.jsp";
				request.setAttribute("message", message);
				request.setAttribute("url", url);
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/files/statusoperacji.jsp");
				dispatcher.forward(request, response);
			}

			if ("dodaj grupe".equals(submit)) {


				request.setAttribute("idklasy", idklasy);
				request.setAttribute("nazwaklasy", nazwa);
				request.setAttribute("url", url);
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/admin/addGroup.jsp");
				dispatcher.forward(request, response);

			}

			if ("zapisz zmiany".equals(submit)) {

				if (!klasajest) {

					cdm.updateClass(nazwa, nowanazwa, logindydaktyka);
					message = "Dane zapisane, nazwa klasy zmieniona z <b>" + nazwa+ "</b> na <b>" + nowanazwa + "</b>";
					request.setAttribute("message", message);
					request.setAttribute("url", url);
					RequestDispatcher dispatcher = getServletContext()
							.getRequestDispatcher("/files/statusoperacji.jsp");
					dispatcher.forward(request, response);
				} else {
					message = "Juz posiadasz klasę o nazwie <b>" + nazwa+ "</b>!";
					request.setAttribute("message", message);
					request.setAttribute("url", url);
					RequestDispatcher dispatcher = getServletContext()
							.getRequestDispatcher("/files/statusoperacji.jsp");
					dispatcher.forward(request, response);
				}

			}
		} catch (Exception e) {
			message = "Wyjątek krytyczny ";
			request.setAttribute("message", message);
			request.setAttribute("url", url);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
		}
	}

}
