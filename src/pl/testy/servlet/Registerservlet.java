package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;
import pl.testy.db.manager.ClassDbManager;
import pl.testy.db.manager.UserDbManager;
import pl.testy.walidator.Walidator;

/**
 * Servlet implementation class Myservlet
 */
@WebServlet("/Registerservlet")
public class Registerservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String message = "";
		String url = "/files/rejestracja.jsp";
		try {

			Walidator walidator = new Walidator();

			// Pobranie danych formularza rejestracji
			String imie = request.getParameter("imie");
			String nazwisko = request.getParameter("nazwisko");
			String login = request.getParameter("login");
			String radio_role = request.getParameter("radio_role");
			String password = request.getParameter("password");
			String re_password = request.getParameter("retypepassword");
			String email = request.getParameter("email");
			String logindydaktyka = request.getParameter("logindydaktyka");
			String nazwaklasy = request.getParameter("nazwaklasy");
			String nazwagrupy = request.getParameter("nazwagrupy");
		
			String role;
			
			ClassDbManager cdm = new ClassDbManager();
			UserDbManager mgm = new UserDbManager();
			mgm.checkUser(login);

			boolean dlugoschasla = walidator.validateLenghtString(password);
			boolean emaildbwalidator = mgm.checkEmail(email);
			boolean imiewalidator = walidator.validateEmptyString(imie);
			boolean nazwiskowalidator = walidator.validateEmptyString(nazwisko);
			boolean re_passwordwalidator = walidator.validatePassword(password,
					re_password);
			boolean emailwalidator = walidator.validateEmail(email);
			int iddydaktyka = mgm.getIdByLogin(logindydaktyka);
			int idklasy = cdm.getIdClassByName(nazwaklasy, iddydaktyka);
			boolean loginwalidator = mgm.checkUser(login);
			boolean klasawalidator = cdm.checkClass(nazwaklasy, logindydaktyka);
			boolean grupawalidator = cdm.checkGroup(nazwagrupy, idklasy);
			
			// Pobranie danych dla recaptcha
			String challengeReCaptcha = request
					.getParameter("recaptcha_challenge_field");
			String responseReCaptcha = request
					.getParameter("recaptcha_response_field");
			String remoteAddr = request.getRemoteAddr(); // Address użytkownika

			ReCaptchaImpl reCaptcha = new ReCaptchaImpl();

			// ustawiamy nasz klucz prywatny
			reCaptcha.setPrivateKey("6LfnstASAAAAALjbJGdRHg2M1NqFynpS-JcbEn6X");

			// sprawdzamy czy użytkownik wpisał pobrany wyraz w recaptcha
			ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(
					remoteAddr, challengeReCaptcha, responseReCaptcha);

			if (!reCaptchaResponse.isValid()) { 

				message += "Niepoprawny kod captcha !";
			}
			System.out.println(emaildbwalidator);
			System.out.println(loginwalidator);
			
			if (loginwalidator) {

				message += "Login istnieje w bazie";

			}
			if (emaildbwalidator) {

				message += "email istnieje w bazie";

			}
			if (!dlugoschasla) {
				message += "Niepoprawny format hasla";
			}
			if (!emailwalidator){
				message += "Niepoprawny format email lub email istnieje w bazie";
				System.out.println(emailwalidator);
			}
			if (reCaptchaResponse.isValid() && imiewalidator
					&& nazwiskowalidator && re_passwordwalidator
					&& !loginwalidator &&!emaildbwalidator &&emailwalidator) {

				if (radio_role.equals("admin_role")) {

					role = "adminx";

					mgm.saveDydaktyk(imie, nazwisko, login, password, email, role 
							);
					cdm.saveDefaultClass(login);
					message = "Dane zapisane, Sprawdź swoją pocztę i kliknij link aktywacyjny w celu aktywacji konta";
				} else {
					
				 loginwalidator = mgm.checkUser(logindydaktyka);
				 
					role = "userx";
					if(loginwalidator && klasawalidator && grupawalidator){
						
						mgm.saveUser(imie, nazwisko, login, password, email, role,
								logindydaktyka,nazwaklasy, nazwagrupy);
						message = "Dane zapisane, Sprawdź swoją pocztę i kliknij link aktywacyjny w celu aktywacji konta";
					} else {
						message += "Podany login , lub klasa, lub grupa dydaktyka nie istnieje !";
					}
				}

			} else {
				message += "<-- Błąd ";
				request.setAttribute("message", message);

			}

		} catch (Exception e) { // przechwycenie wyjatkow
			message = "Wyjątek Krytyczny"; // TODO: handle exception
			request.setAttribute("message", message);
		}

		finally { // ten kod wykona sie nawet jezeli bedzie wyjatek
			request.setAttribute("url", url);
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response); // TODO: handle exception
		}

	}
}
