package pl.testy.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.SurveyDbManager;
import pl.testy.model.Odpowiedz;
import pl.testy.model.Pytanie;

/**
 * Servlet implementation class maketestservlet
 */
@WebServlet("/admin/makesurveyservlet")
public class Makesurveyservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	// metoda do ustawiania atrybutow
	private void setAllAttribute(HttpServletRequest request) {

		Integer iloscpytan = Integer.parseInt(request.getParameter("ilosc"));

		request.setAttribute("iloscpytan", iloscpytan);

		for (int i = 0; i < iloscpytan; i++) {
			Integer iloscodpowiedzi = Integer.parseInt(request
					.getParameter("iloscodpowiedzi" + i));
			request.setAttribute("iloscodpowiedzi" + i, iloscodpowiedzi);
		}
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		setAllAttribute(request);
		String message = "";
		String url = "/admin/index.jsp";
		String submit = request.getParameter("submit");
		Integer iloscpytan = Integer.parseInt(request.getParameter("ilosc"));

		try {

			if ("dodaj pytanie".equals(submit)) {

				request.setAttribute("iloscpytan", iloscpytan + 1);

				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/admin/makeSurvey.jsp");
				dispatcher.forward(request, response);

			}

			for (int i = 0; i < iloscpytan; i++) {

				String submit_add_anserw = request
						.getParameter("submit_add_anserw" + i);
				String submit_delete_anserw = request
						.getParameter("submit_delete_anserw" + i);

				if (submit_add_anserw != null) {
					Integer iloscodpowiedzi = Integer.parseInt(request
							.getParameter("iloscodpowiedzi" + i));
					request.setAttribute("iloscodpowiedzi" + i,
							iloscodpowiedzi + 1);

					RequestDispatcher dispatcher = getServletContext()
							.getRequestDispatcher("/admin/makeSurvey.jsp");
					dispatcher.forward(request, response);

				}
				if (submit_delete_anserw != null) {

					Integer iloscodpowiedzi = Integer.parseInt(request
							.getParameter("iloscodpowiedzi" + i));
					request.setAttribute("iloscodpowiedzi" + i,
							iloscodpowiedzi - 1);

					RequestDispatcher dispatcher = getServletContext()
							.getRequestDispatcher("/admin/makeSurvey.jsp");
					dispatcher.forward(request, response);

				}

			}
			if ("usun pytanie".equals(submit)) {

				request.setAttribute("iloscpytan", iloscpytan - 1);
				request.setAttribute("ilosc", iloscpytan - 1);
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/admin/makeSurvey.jsp");
				dispatcher.forward(request, response);

			}

			if ("zapisz zmiany".equals(submit)) {

				String nazwa = request.getParameter("f_name");

				String logindydaktyka = request.getUserPrincipal().getName();
				List<Pytanie> listapytan = new ArrayList<Pytanie>();

				for (int i = 0; i < iloscpytan; i++) {
					Pytanie p = new Pytanie();
					List<Odpowiedz> listaodpowiedzi = new ArrayList<Odpowiedz>();
					p.setTresc(request.getParameter("f_question[" + i + "]"));
					p.setFoto(request.getParameter("foto[" + i + "]"));
					p.setVideo(request.getParameter("video[" + i + "]"));

					Integer iloscodpowiedzi = Integer.parseInt(request
							.getParameter("iloscodpowiedzi" + i));
					for (int j = 0; j < iloscodpowiedzi; j++) {
						Odpowiedz o = new Odpowiedz();
						o.setTresc(request.getParameter("f_answer[" + i + "]["
								+ j + "]"));
						listaodpowiedzi.add(o);
					}

					p.setListaodpowiedz(listaodpowiedzi);
					listapytan.add(p);

				}

				SurveyDbManager SurveyDbManager = new SurveyDbManager();
				SurveyDbManager.saveSurvey(nazwa, logindydaktyka, listapytan);

			
				message = "dane zapisane";
				request.setAttribute("url", url);
				request.setAttribute("message", message);
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/files/statusoperacji.jsp");
				dispatcher.forward(request, response);

			}
		} catch (Exception e) {
			e.printStackTrace();
			message += e;
			request.setAttribute("url", url);
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);// TODO: handle exception
		} 
	}

}
