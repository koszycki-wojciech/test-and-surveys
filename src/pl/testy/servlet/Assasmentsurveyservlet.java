package pl.testy.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.SurveyDbManager;
import pl.testy.model.Pytanie;
import pl.testy.walidator.AuthCheck;

/**
 * Servlet implementation class Asassmenttestservlet
 */
@WebServlet("/user/assasmentsurveyservlet")
public class Assasmentsurveyservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Assasmentsurveyservlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("id"));
		String login = request.getUserPrincipal().getName();
		String message = "";
		
		AuthCheck acheck = new AuthCheck();
		if (!acheck.checkSurveyPermisson(login, id)) {

			message = "Brak dostępu !!!";
			String url = "/index.jsp";
			request.setAttribute("url", url);
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		SurveyDbManager sdm = new SurveyDbManager();
		List<Integer> listaid = sdm.getIdPytan(id);
		
	
		
		Pytanie p = sdm.getPytanie(listaid.get(0));

		
		request.setAttribute("listapytan", listaid);
		request.setAttribute("pytanie", p);
		request.setAttribute("indexpytanie", 0);
		request.setAttribute("udzieloneodpowiedzi", new ArrayList<Integer>());
		request.setAttribute("testId", id);

		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/user/startSurvey.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		
		SurveyDbManager sdm = new SurveyDbManager();
		
		int testId = Integer.parseInt(request.getParameter("testId"));
		int indexpytanie = Integer.parseInt(request
				.getParameter("indexpytanie"));
		int iloscodpowiedzi = Integer.parseInt(request
				.getParameter("iloscodpowiedzi"));
		int iloscpytan = Integer.parseInt(request.getParameter("iloscpytan"));
		String czas = request.getParameter("czas");

		List<Integer> listaPytan = new ArrayList<Integer>();
		for (int i = 0; i < iloscpytan; i++) {
			int pytanieId = Integer.parseInt(request.getParameter("idpytania_"
					+ i));
			listaPytan.add(pytanieId);
		}

		List<Integer> listaOdp = new ArrayList<Integer>();
		for (int i = 0; i < iloscodpowiedzi; i++) {
			int odpId = Integer.parseInt(request.getParameter("idodpowiedzi_"
					+ i));
			listaOdp.add(odpId);
		}

		int odpNaPytanie = Integer.parseInt(request.getParameter("odpowiedz"));
		listaOdp.add(odpNaPytanie);
		indexpytanie++;

		
		if (indexpytanie == listaPytan.size()) {


		
			int wynik = 0;


			sdm.surveyFinish(listaOdp);

			request.setAttribute("wynik", wynik);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/user/wynikAnkieta.jsp");
			dispatcher.forward(request, response);

			return;
		}

		Pytanie p = sdm.getPytanie(listaPytan.get(indexpytanie));

		request.setAttribute("czas", czas);
		request.setAttribute("testId", testId);
		request.setAttribute("listapytan", listaPytan);
		request.setAttribute("pytanie", p);
		request.setAttribute("indexpytanie", indexpytanie);
		request.setAttribute("udzieloneodpowiedzi", listaOdp);

		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/user/startSurvey.jsp");
		dispatcher.forward(request, response);
	}

}
