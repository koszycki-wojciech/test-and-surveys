package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.UserDbManager;
import pl.testy.model.UserFull;
import pl.testy.service.NotificationService;

/**
 * Servlet implementation class Remindpasswordservlet
 */
@WebServlet("/Remindpasswordservlet")
public class Remindpasswordservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Remindpasswordservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String token = request.getParameter("token");

		UserDbManager userDbManager = new UserDbManager();
		boolean result = userDbManager.changePassword(token);
		String url = "/index.jsp";
		String message = "";
		if (result) {
			message = "Mail z hasłem został wysłany";
		} else {
			message = "Niepoprawny token";
		}
		request.setAttribute("url", url);
		request.setAttribute("message", message);
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/files/statusoperacji.jsp");
		dispatcher.forward(request, response);
	}
		
		// TODO Auto-generated method stub
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String  login = request.getParameter("login");
		UserDbManager udm = new UserDbManager();
		String message = "Email do resetujący hasło został wysłany ";
		String url = "/index.jsp";
		boolean loginjest = udm.checkUser(login);
		
		if(loginjest){
			
			int iduser = udm.getIdByLogin(login);
			UserFull uf = udm.getProfileById(iduser);
			
			String email = uf.getEmail();
			NotificationService ns = new NotificationService();
			String token = ns.createPasswordChangeMail(email, login);
			

			udm.saveToken(token, login);
			request.setAttribute("url", url);
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
			
		} else {
			message = "Login nie istnieje w bazie";
	
			request.setAttribute("url", url);
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
		}
		
		
		
		
		
		// TODO Auto-generated method stub
	}

}
