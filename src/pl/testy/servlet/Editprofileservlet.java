package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.UserDbManager;
import pl.testy.model.UserFull;
import pl.testy.walidator.Walidator;

/**
 * Servlet implementation class Editservlet
 */
@WebServlet("/editprofileservlet")
public class Editprofileservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Editprofileservlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		UserDbManager mgm = new UserDbManager(); // tworzy obiekt klasy
													// UserDbManager
		UserFull uf = mgm.getUserById(Integer.parseInt(request
				.getParameter("id"))); // wywoluje metode wszystko co przychodzi
										// w request jest w formie string->
										// parse

		request.setAttribute("useredit", uf);
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/admin/editUsers.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String message = "";
		String url = "/index.jsp";
		// Pobranie danych formularza 
		String imie = request.getParameter("imie");
		String nazwisko = request.getParameter("nazwisko");
		String email = request.getParameter("email");
		String login = request.getUserPrincipal().getName();
		try {
			Walidator walidator = new Walidator();
			
			UserDbManager udm = new UserDbManager();
			int iduser = udm.getIdByLogin(login);

			
			boolean imiewalidator = walidator.validateEmptyString(imie);
			boolean nazwiskowalidator = walidator.validateEmptyString(nazwisko);
			boolean emailwalidator = walidator.validateEmail(email);
			boolean emaildbwalidator = udm.checkEmail(email);
			
			
			
			if (emaildbwalidator) {

				message += "  Błąd Podany email istnieje w bazie";
			}
			if (imiewalidator && nazwiskowalidator) {

				message += " Błąd Pola wymagane";
			}
			if (emailwalidator) {

				message += "  Błąd Niepoprawny format email";
			}

			udm.updateProfile(iduser, imie, nazwisko, email);
			message = "Dane zapisane..";
			
		} catch (Exception e) { // przechwycenie wyjatkow
			message = "Wyjątek Krytyczny"; // TODO: handle exception
		}

		finally { // ten kod wykona sie nawet jezeli bedzie wyjatek
			request.setAttribute("message", message);
			request.setAttribute("url", url);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response); // TODO: handle exception
		}
	}
}
