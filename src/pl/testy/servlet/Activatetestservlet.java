package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.TestDbManager;
import pl.testy.model.TestHeader;
import pl.testy.walidator.AuthCheck;

/**
 * Servlet implementation class Activatetestservlet
 */
@WebServlet("/admin/activatetestservlet")
public class Activatetestservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Activatetestservlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		int idtestu = Integer.parseInt(request.getParameter("id"));
		
		TestDbManager tdm = new TestDbManager();
		TestHeader th = tdm.showTest(idtestu);

		request.setAttribute("testedit", th);
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/admin/editTest.jsp");
		dispatcher.forward(request, response);

		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String message = "";
		String url = "/admin/showTests.jsp";
		String nazwa = request.getParameter("nazwa");
		String opis = request.getParameter("opis");
		String stan = request.getParameter("stan");
		
		
		

		
		int duration = Integer.parseInt(request.getParameter("duration"));
		int maxpunkty = Integer.parseInt(request.getParameter("maxpunkty"));
		int idtest = Integer.parseInt(request.getParameter("idtestu"));
		
		try {
			
			//Update danych testu
			TestDbManager tdm = new TestDbManager();
			tdm.updateTest(idtest, nazwa, opis, stan, duration, maxpunkty);
			
			message = "Dane zapisane..";
			request.setAttribute("message", message);
			request.setAttribute("url", url);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			
			message = "Błąd";
			request.setAttribute("message", message);
			request.setAttribute("url", url);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);// TODO: handle exception
		}
		
		// TODO Auto-generated method stub
	}

}
