package pl.testy.db.manager;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pl.testy.converter.MD5Genegate;
import pl.testy.model.GrupaKlasa;
import pl.testy.model.Plik;
import pl.testy.model.UserFull;
import pl.testy.model.UserHeader;
import pl.testy.service.NotificationService;
/**
 * Klasa obsługująca wymiane informacji z bazą danych dla użytkowników i informacji z nimi związanych
 * @author wojtek
 *
 */
public class UserDbManager extends Dbmanager {
/**
 * Zapisywanie użytkownika tester do bazy danych
 * @param imie imie
 * @param nazwisko nazwisko
 * @param login login
 * @param password hasło
 * @param email adres email
 * @param role rola użytkownika
 * @param logindydaktyka login dydaktyka
 * @param nazwaklasy nazwa klasy
 * @param nazwagrupy nazwa grupy
 */
	public void saveUser(String imie, String nazwisko, String login,
			String password, String email, String role, String logindydaktyka,
			String nazwaklasy, String nazwagrupy) {
		try {

			MD5Genegate md5Genegate = new MD5Genegate();
			password = md5Genegate.generate(password);

			// Zapisywanie do tabeli users
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("INSERT INTO users (imie,nazwisko,haslo,email,stan,role,login) VALUES (?,?,?, ?,'N',?,?);");
			statement.setString(1, imie);
			statement.setString(2, nazwisko);
			statement.setString(3, password);
			statement.setString(4, email);
			statement.setString(5, role);
			statement.setString(6, login);
			statement.execute();
			statement.close();

			// Zapytanie o id przez logindydaktyka
			int dydaktykid = getIdByLogin(logindydaktyka);
			int testerid = getIdByLogin(login);

			PreparedStatement statement3 = connect
					.prepareStatement("INSERT INTO role (id_testera, id_dydaktyka) VALUES (?,?);");
			statement3.setInt(1, testerid);
			statement3.setInt(2, dydaktykid);
			statement3.execute();
			statement3.close();

			// BEGIN
			NotificationService notificationService = new NotificationService();
			String token = notificationService.createAcceptNotification(email,
					login);

			statement = connect
					.prepareStatement("INSERT INTO mail_token (token, id_users) VALUES (?,?);");
			statement.setString(1, token);
			statement.setInt(2, testerid);
			statement.execute();

			// END
			statement.close();
			

			ClassDbManager cdm = new ClassDbManager();

			int idklasy = cdm.getIdClassByName(nazwaklasy, dydaktykid);
			int idgrupy = cdm.getIdGroupByName(nazwagrupy, idklasy);

			PreparedStatement statement4 = connect
					.prepareStatement("INSERT INTO tester_grupa (id_testera, id_grupy) VALUES (?,?);");
			statement4.setInt(1, testerid);
			statement4.setInt(2, idgrupy);
			statement4.execute();
			statement4.close();
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
/**
 * Zapisywanie pliku do bazy
 * @param plik ciąg bajtów
 * @param login login dydaktyka
 * @param nazwa nazwa pliku
 */
	public void saveFile(InputStream plik, String login,String nazwa,String typ) {
		
		int iddydaktyk = getIdByLogin(login);
		

		
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("INSERT INTO pliki (id_dydaktyka,nazwa,plik,typ) VALUES (?,?,?,?);");

			statement.setInt(1, iddydaktyk);
			statement.setString(2, nazwa);
			statement.setBlob(3, plik);
			statement.setString(4, typ);
			statement.execute();
			statement.close();
			connect.close();
		} catch (Exception e) {
			e.printStackTrace();// TODO: handle exception
		}

	}
/**
 * Pobieranie pliku z bazy
 * @return plik typu blob
 */
	public Plik getFile(int idplik) {
		
		Plik plikblob = new Plik();
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT plik, nazwa,typ FROM pliki WHERE id_pliki = ? ");
			
			statement.setInt(1, idplik);
			ResultSet wynik = statement.executeQuery();
			
			if (wynik.next()) {
				plikblob.setPlikblob(wynik.getBlob("plik"));
				plikblob.setNazwa(wynik.getString("nazwa"));
				plikblob.setTyp(wynik.getString("typ"));
				
			}

			statement.close();
			connect.close();
			
			return plikblob;
		} catch (Exception e) {
			return plikblob;// TODO: handle exception
		}

	}
/**
 * Zapisywanie dydaktyka do bazy
 * @param imie imie
 * @param nazwisko nazwisko
 * @param login login dydaktyka
 * @param password hasło
 * @param email adres email
 * @param role rola
 */
	public void saveDydaktyk(String imie, String nazwisko, String login,
			String password, String email, String role) {
		try {

			MD5Genegate md5Genegate = new MD5Genegate();
			password = md5Genegate.generate(password);

			// Zapisywanie do tabeli users
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("INSERT INTO users (imie,nazwisko,haslo,email,stan,role,login) VALUES (?,?,?, ?,'N',?,?);");
			statement.setString(1, imie);
			statement.setString(2, nazwisko);
			statement.setString(3, password);
			statement.setString(4, email);
			statement.setString(5, role);
			statement.setString(6, login);
			statement.execute();
			statement.close();

			int iddydaktyk = getIdByLogin(login);

			// BEGIN
			NotificationService notificationService = new NotificationService();
			String token = notificationService.createAcceptNotification(email,
					login);

			statement = connect
					.prepareStatement("INSERT INTO mail_token (token, id_users) VALUES (?,?);");
			statement.setString(1, token);
			statement.setInt(2, iddydaktyk);
			statement.execute();

			// END
			statement.close();
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
/**
 * Zapisywanie wygenerowanego tokenu przy rejestracji
 * @param token token wygenerowany przy rejestracji
 * @param login login uzytkownika
 */
	public void saveToken(String token, String login) {

		int iduser = getIdByLogin(login);

		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("INSERT INTO mail_token (token, id_users) VALUES (?,?);");
			statement.setString(1, token);
			statement.setInt(2, iduser);
			statement.execute();
			statement.close();
			connect.close();
		} catch (Exception e) {
			e.printStackTrace();// TODO: handle exception
		}

	}

	/**
	 * Metoda sprawdzajaca czy login juz istnieje w bazie
	 * @param login
	 * @return wartośc logiczną true dla istniejącego loginu
	 */
	public boolean checkUser(String login) {

		boolean loginjest = false;
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT login FROM users WHERE login=?;");
			statement.setString(1, login);

			ResultSet wynik = statement.executeQuery();
			if (wynik.next()) {
				loginjest = true;
			}
			statement.close();
			connect.close();
			return loginjest;
		} catch (SQLException e) {
			e.printStackTrace();
			return loginjest;
		}

	}
/**
 * Uaktualnienie hasła
 * @param iduser identyfikator użytkownika
 * @param password nowe hasło
 */
	public void updatePassword(int iduser, String password) {

		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("Update users SET haslo=? WHERE id_users=?;");
			statement.setString(1, password);
			statement.setInt(2, iduser);
			statement.execute();

			statement.close();
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();

		}

	}
/**
 * Metoda sprawdzajaca czy użytkownik przynależy do dydaktyka 
 * @param iddydaktyk identyfikator dydaktyka
 * @param iduser identyfikator użytkownika
 * @return wartośc logiczną true jeżeli przynależy
 */
	public boolean checkUserBelongsToAdmin(int iddydaktyk, int iduser) {

		boolean nazwajest = false;
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT id_role FROM role WHERE id_testera=? AND id_dydaktyka=?;");
			statement.setInt(1, iduser);
			statement.setInt(2, iddydaktyk);

			ResultSet wynik = statement.executeQuery();
			if (wynik.next()) {
				nazwajest = true;
			}
			statement.close();
			connect.close();
			return nazwajest;
		} catch (SQLException e) {
			e.printStackTrace();
			return nazwajest;
		}

	}
/**
 * Metoda sprawdzajaca czy dany adres email istnieje w bazie
 * @param email adres email
 * @return wartośc logiczną true jeżeli istnieje
 */
	public boolean checkEmail(String email) {

		boolean loginjest = false;
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT email FROM users WHERE email=?;");
			statement.setString(1, email);

			ResultSet wynik = statement.executeQuery();
			if (wynik.next()) {
				loginjest = true;
			}
			statement.close();
			connect.close();
			return loginjest;
		} catch (SQLException e) {
			e.printStackTrace();
			return loginjest;
		}

	}

	/**
	 * Metoda Zwracająca id uzytkownika wprowadzajac login jako parametr
	 * @param login login użytkownika
	 * @return identyfikator użytkownika
	 */
	public int getIdByLogin(String login) {
		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT id_users FROM users WHERE login = ?;");
			statement.setString(1, login);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			int id = 0;

			if (wynik.next()) { // przeszukiwanie wierszy

				id = wynik.getInt("id_users"); // jezeli cos znajdzie to zwroci
												// "id_users"

			}
			statement.close();
			wynik.close();
			connect.close();
			return id;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}

	}

	// metoda zwracajaca liste z ogolnymi danymi uzytkownika tester parametr
	// wejsciowy login Dydaktyka ktory to wyswietla
	/**
	 * Metoda zwracajaca listę z ogolnymi danymi użytkownika tester 
	 * @param logindydaktyka login dydaktyka
	 * @return lista obiektu UserHeader
	 */
	public List<UserHeader> userlist(String logindydaktyka) {

		List<UserHeader> userlist = new ArrayList<UserHeader>();

		try {
			Connection connect = startConnections();

			int iddydaktyka = getIdByLogin(logindydaktyka);

			PreparedStatement statement = connect
					.prepareStatement("SELECT u.id_users, u.imie, u.nazwisko, u.email, u.stan FROM users u, role r WHERE u.id_users = r.id_testera AND r.id_dydaktyka =?");
			statement.setInt(1, iddydaktyka);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy

				UserHeader uh = new UserHeader();

				PreparedStatement statement2 = connect
						.prepareStatement("SELECT g.nazwa nazwagrupa, k.nazwa nazwaklasy FROM grupy g, klasy k, tester_grupa tg WHERE tg.id_testera=? AND tg.id_grupy=g.id_grupy AND k.id_dydaktyka =? AND k.id_klasy=g.id_klasy;");
				statement2.setInt(1, wynik.getInt("id_users"));
				statement2.setInt(2, iddydaktyka);

				ResultSet wynik2 = statement2.executeQuery();

				List<GrupaKlasa> gklista = new ArrayList<GrupaKlasa>();

				while (wynik2.next()) {

					GrupaKlasa gk = new GrupaKlasa();
					gk.setNazwagrupy(wynik2.getString("nazwagrupa"));
					gk.setNazwaklasy(wynik2.getString("nazwaklasy"));
					gklista.add(gk);
				}
				uh.setListagrupaklasa(gklista);
				uh.setId(wynik.getInt("id_users"));
				uh.setImie(wynik.getString("imie"));
				uh.setNazwisko(wynik.getString("nazwisko"));
				uh.setEmail(wynik.getString("email"));
				uh.setStan(wynik.getString("stan"));

				userlist.add(uh); // dorzucanie do listy
			}
			statement.close();
			wynik.close();
			connect.close();
			return userlist;

		} catch (SQLException e) {
			e.printStackTrace();
			return userlist;
		}
	}

	/**
	 * Pobranie wszystkich danych o danym użytkowniku tester
	 * @param identyfikator
	 * @return obiekt UserFull
	 */
	// uzytkownika
	public UserFull getUserById(int id)

	{

		UserFull uf = new UserFull();

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT u.id_users, u.imie, u.nazwisko, u.email, u.login, u.stan, g.nazwa nazwagrupa, k.nazwa nazwaklasy FROM users u, grupy g, klasy k, tester_grupa tg WHERE u.id_users=?  AND tg.id_grupy=g.id_grupy AND k.id_klasy=g.id_klasy;");

			statement.setInt(1, id);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			if (wynik.next()) { // przeszukiwanie wierszy

				uf.setLogin(wynik.getString("login"));
				uf.setId(wynik.getInt("id_users"));
				uf.setImie(wynik.getString("imie"));
				uf.setNazwisko(wynik.getString("nazwisko"));
				uf.setEmail(wynik.getString("email"));
				uf.setStan(wynik.getString("stan"));
				uf.setKlasa(wynik.getString("nazwaklasy"));
				uf.setGrupa(wynik.getString("nazwagrupa"));

			}
			statement.close();
			wynik.close();
			connect.close();
			return uf;

		} catch (SQLException e) {
			e.printStackTrace();
			return uf;

		}
	}
	public List<Plik> fileList(String logindydaktyka) {

		List<Plik> fileList = new ArrayList<Plik>();

		try {
			Connection connect = startConnections();

			UserDbManager mgm = new UserDbManager();

			int iddydaktyka = mgm.getIdByLogin(logindydaktyka);

			PreparedStatement statement = connect
					.prepareStatement("SELECT id_pliki, id_dydaktyka, nazwa,typ FROM pliki WHERE id_dydaktyka =?;");
			statement.setInt(1, iddydaktyka);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy

				Plik p = new Plik();

				p.setIddydaktyk(iddydaktyka);
				p.setIdplik(wynik.getInt("id_pliki"));
				p.setNazwa(wynik.getString("nazwa"));
				p.setTyp(wynik.getString("typ"));
			
				fileList.add(p); // dorzucanie do listy
			}
			statement.close();
			wynik.close();
			connect.close();
			return fileList;

		} catch (SQLException e) {
			e.printStackTrace();
			return fileList;
		}
	}
/**
 * Pobranie wszystkich danych o danym użytkowniku dydaktyk
 * @param id identyfikator użytkownika
 * @return obiekt UserFull
 */
	public UserFull getProfileById(int id)

	{

		UserFull uf = new UserFull();

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT u.id_users, u.imie, u.nazwisko, u.email, u.login, u.stan FROM users u WHERE u.id_users=?;");

			statement.setInt(1, id);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			if (wynik.next()) { // przeszukiwanie wierszy

				uf.setLogin(wynik.getString("login"));
				uf.setId(wynik.getInt("id_users"));
				uf.setImie(wynik.getString("imie"));
				uf.setNazwisko(wynik.getString("nazwisko"));
				uf.setEmail(wynik.getString("email"));
				uf.setStan(wynik.getString("stan"));

			}
			statement.close();
			wynik.close();
			connect.close();
			return uf;

		} catch (SQLException e) {
			e.printStackTrace();
			return uf;

		}
	}
/**
 * Pobieranie hasła
 * @param id identyfikator
 * @return hasło jako ciąg znaków
 */
	public String getPassword(int id)

	{

		String password = "";
		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT u.haslo FROM users u WHERE u.id_users=?;");

			statement.setInt(1, id);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			if (wynik.next()) { // przeszukiwanie wierszy

				password = wynik.getString("u.haslo");

			}
			statement.close();
			wynik.close();
			connect.close();
			return password;

		} catch (SQLException e) {
			e.printStackTrace();
			return password;

		}
	}
/**
 * Uaktualnienie danych użytkownika tester
 * @param iduser identyfikator użytkownika
 * @param iddydaktyk identyfikator jego dydaktyka
 * @param imie imie
 * @param nazwisko nazwisko
 * @param email adres email
 * @param login login
 * @param klasa klasa
 * @param grupa grupa
 */
	public void updateUser(int iduser, int iddydaktyk, String imie,
			String nazwisko, String email, String login, String klasa,
			String grupa) {
		try {
			int idklasa = 0;
			int idgrupa = 0;

			ClassDbManager cdm = new ClassDbManager();
			idklasa = cdm.getIdClassByName(klasa, iddydaktyk);
			idgrupa = cdm.getIdGroupByName(grupa, idklasa);

			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("UPDATE users u, tester_grupa tg, grupy g SET u.imie=?, u.nazwisko=?, u.email=?, u.login=?, tg.id_grupy=?  WHERE u.id_users=?  AND tg.id_testera=? AND g.id_klasy=?;");
			statement.setString(1, imie);
			statement.setString(2, nazwisko);
			statement.setString(3, email);
			statement.setString(4, login);
			statement.setInt(5, idgrupa);
			statement.setInt(6, iduser);
			statement.setInt(7, iduser);
			statement.setInt(8, idklasa);
			statement.execute();
			statement.close();
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();// TODO: handle exception
		}
	}
/**
 * Uaktualnienie danych profilowych
 * @param iduser identyfikator
 * @param imie imie
 * @param nazwisko nazwiskop
 * @param email adres email
 */
	public void updateProfile(int iduser, String imie, String nazwisko,
			String email) {
		try {

			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("UPDATE users u SET u.imie=?, u.nazwisko=?, u.email=? WHERE u.id_users=?;");
			statement.setString(1, imie);
			statement.setString(2, nazwisko);
			statement.setString(3, email);
			statement.setInt(4, iduser);
			statement.execute();
			statement.close();
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();// TODO: handle exception
		}

	}
/**
 * Pobranie identyfikatora dydaktyka 
 * @param login login użytkownika przynależącego
 * @return identyfikator
 */
	public int getIdDydaktykByTesterLogin(String login) {
		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT r.id_dydaktyka FROM users u,role r  WHERE u.login=? AND u.id_users=r.id_testera;");
			statement.setString(1, login);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			int id = 0;

			if (wynik.next()) { // przeszukiwanie wierszy

				id = wynik.getInt("r.id_dydaktyka"); // jezeli cos znajdzie to
														// zwroci
				// "id_users"

			}
			statement.close();
			wynik.close();
			connect.close();
			return id;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}

	}
/**
 * Aktywacja użytkownika
 * @param token token podany przez użytkownika
 * @return wartość logiczna false jeżeli podany token jest nieprawidłowy
 */
	public boolean acceptUserToken(String token) {

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT id_users FROM mail_token WHERE token = ?;");
			statement.setString(1, token);
			ResultSet wynik = statement.executeQuery();

			int id = 0;

			if (wynik.next()) {

				id = wynik.getInt("id_users");

			} else {
				return false;
			}

			String rola = getRoleByIdUsers(id);
			statement = connect
					.prepareStatement("UPDATE users SET stan = 'A', role = ? WHERE id_users = ?;");
			statement.setString(1, rola);
			statement.setInt(2, id);
			statement.execute();

			statement.close();
			wynik.close();
			connect.close();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
/**
 * Uaktualnienie hasła
 * @param token token
 * @return wartość logiczna false jeżeli podany token jest nieprawidłowy
 */
	public boolean changePassword(String token) {

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT id_users FROM mail_token WHERE token = ?;");
			statement.setString(1, token);
			ResultSet wynik = statement.executeQuery();
			String email = "";
			String password = "";
			int id = 0;

			Random r = new Random();
			for (int i = 0; i < 6; i++) {

				password += r.nextInt(9);
			}

			String passwordmd5 = password;

			MD5Genegate md5 = new MD5Genegate();
			passwordmd5 = md5.generate(password);

			if (wynik.next()) {

				id = wynik.getInt("id_users");

			} else {
				return false;
			}

			statement = connect
					.prepareStatement("UPDATE users SET haslo = ? WHERE id_users = ?;");
			statement.setString(1, passwordmd5);
			statement.setInt(2, id);
			statement.execute();

			statement.close();
			wynik.close();

			PreparedStatement statement1 = connect
					.prepareStatement("SELECT email FROM users WHERE id_users = ?;");
			statement1.setInt(1, id);
			ResultSet wynik2 = statement1.executeQuery();

			if (wynik2.next()) {

				email = wynik2.getString("email");
			}

			NotificationService ns = new NotificationService();
			ns.sendNewPasswordMail(email, password);
			wynik2.close();
			connect.close();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
/**
 * Pobieranie roli użytkownika
 * @param idusers identyfikator uzytkownika
 * @return rola
 */
	public String getRoleByIdUsers(int idusers) {

		String role = "";

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT role FROM users WHERE id_users = ?;");
			statement.setInt(1, idusers);
			ResultSet wynik = statement.executeQuery();

			if (wynik.next()) {

				role = wynik.getString("role");

				if ("adminx".equals(role)) {
					role = "admin";
				} else {
					role = "user";
				}

			}
			return role;
		} catch (Exception e) {
			return role;// TODO: handle exception
		}

	}
/**
 * Usunięcie użytkownika
 * @param iduser identyfikator uzytkownika
 */
	public void deleteUser(int iduser) {
		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("DELETE FROM users WHERE id_users=? ;");
			statement.setInt(1, iduser);
			statement.execute();

		} catch (Exception e) {

		}
	}
/**
 * Usunięcie wszystkich użytkowników
 */
	public void deleteAllUsers() {
		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("DELETE FROM users ;");
			statement.execute();

		} catch (Exception e) {

		}
	}
}
