package pl.testy.model;


public class ClassHeader {

	private int idklasy;
	private String nazwaklasy;
	
	public int getIdklasy() {
		return idklasy;
	}
	public void setIdklasy(int idklasy) {
		this.idklasy = idklasy;
	}
	public String getNazwaklasy() {
		return nazwaklasy;
	}
	public void setNazwaklasy(String nazwaklasy) {
		this.nazwaklasy = nazwaklasy;
	}
}
