package pl.testy.model;

import java.util.List;

public class Pytanie {

	private String tresc;
	private String foto;
	private String video;
	private List<Odpowiedz> listaodpowiedz; //typ lista 
	public String getTresc() {
		return tresc;
	}
	public void setTresc(String tresc) {
		this.tresc = tresc;
	}
	public List<Odpowiedz> getListaodpowiedz() {
		return listaodpowiedz;
	}
	public void setListaodpowiedz(List<Odpowiedz> listaodpowiedz) {
		this.listaodpowiedz = listaodpowiedz;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getVideo() {
		return video;
	}
	public void setVideo(String video) {
		this.video = video;
	}
}
