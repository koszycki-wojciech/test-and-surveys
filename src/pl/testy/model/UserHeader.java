package pl.testy.model;

import java.util.List;

public class UserHeader {

	private int id;
	private String imie;
	private String nazwisko;
	private String email;
	private String stan;
	private List<GrupaKlasa> listagrupaklasa;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStan() {
		return stan;
	}
	public void setStan(String stan) {
		this.stan = stan;
	}
	public List<GrupaKlasa> getListagrupaklasa() {
		return listagrupaklasa;
	}
	public void setListagrupaklasa(List<GrupaKlasa> listagrupaklasa) {
		this.listagrupaklasa = listagrupaklasa;
	}
	public String wypiszklase() {
		String x = "";
		for (GrupaKlasa gk : listagrupaklasa) {
			x+= gk.getNazwaklasy();
		}
		return x; 
	}
	public String wypiszgrupy() {
		String x = "";
		for (GrupaKlasa gk : listagrupaklasa) {
			x+= gk.getNazwagrupy();
		}
		return x; 
	}
	
}
