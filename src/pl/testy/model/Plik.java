package pl.testy.model;

import java.sql.Blob;

public class Plik {

	private int idplik;
	private int iddydaktyk;
	private String nazwa;
	private String typ;
	private Blob plikblob;
	public int getIdplik() {
		return idplik;
	}
	public void setIdplik(int idplik) {
		this.idplik = idplik;
	}
	public int getIddydaktyk() {
		return iddydaktyk;
	}
	public void setIddydaktyk(int iddydaktyk) {
		this.iddydaktyk = iddydaktyk;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public Blob getPlikblob() {
		return plikblob;
	}
	public void setPlikblob(Blob plikblob) {
		this.plikblob = plikblob;
	}
	
}
