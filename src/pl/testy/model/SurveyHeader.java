package pl.testy.model;

public class SurveyHeader {
	private int idankiety;
	private String nazwa;
	private String stan;
	private int iloscpytan;
	
	public int getIdankiety() {
		return idankiety;
	}
	public void setIdankiety(int idankiety) {
		this.idankiety = idankiety;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public String getStan() {
		return stan;
	}
	public void setStan(String stan) {
		this.stan = stan;
	}
	public int getIloscpytan() {
		return iloscpytan;
	}
	public void setIloscpytan(int iloscpytan) {
		this.iloscpytan = iloscpytan;
	}
	

}
