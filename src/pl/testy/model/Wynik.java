package pl.testy.model;

import java.sql.Date;

public class Wynik {
	
	private int idtestu;
	private int idwyniku;
	private int maxpunkty; 
	private String nazwatestu;
	private String imie;
	private String nazwisko;
	private int zdobytywynik;
	private Date datastart;
	private Date datastop;
	public int getIdtestu() {
		return idtestu;
	}
	public void setIdtestu(int idtestu) {
		this.idtestu = idtestu;
	}

	public int getZdobytywynik() {
		return zdobytywynik;
	}
	public void setZdobytywynik(int zdobytywynik) {
		this.zdobytywynik = zdobytywynik;
	}
	public Date getDatastart() {
		return datastart;
	}
	public void setDatastart(Date datastart) {
		this.datastart = datastart;
	}
	public Date getDatastop() {
		return datastop;
	}
	public void setDatastop(Date datastop) {
		this.datastop = datastop;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwatestu() {
		return nazwatestu;
	}
	public void setNazwatestu(String nazwatestu) {
		this.nazwatestu = nazwatestu;
	}
	public int getMaxpunkty() {
		return maxpunkty;
	}
	public void setMaxpunkty(int maxpunkty) {
		this.maxpunkty = maxpunkty;
	}
	public int getIdwyniku() {
		return idwyniku;
	}
	public void setIdwyniku(int idwyniku) {
		this.idwyniku = idwyniku;
	}
}
