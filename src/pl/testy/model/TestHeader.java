package pl.testy.model;

public class TestHeader {
	private int idtestu;
	private String nazwa;
	private String opis;
	private String stan;
	private int czastrwania;
	private int iloscpytantestu;
	private int maxpunkty;
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getStan() {
		return stan;
	}
	public void setStan(String stan) {
		this.stan = stan;
	}
	public int getCzastrwania() {
		return czastrwania;
	}
	public void setCzastrwania(int czastrwania) {
		this.czastrwania = czastrwania;
	}
	public int getIdtestu() {
		return idtestu;
	}
	public void setIdtestu(int idtestu) {
		this.idtestu = idtestu;
	}
	public int getIloscpytantestu() {
		return iloscpytantestu;
	}
	public void setIloscpytantestu(int iloscpytantestu) {
		this.iloscpytantestu = iloscpytantestu;
	}
	public int getMaxpunkty() {
		return maxpunkty;
	}
	public void setMaxpunkty(int maxpunkty) {
		this.maxpunkty = maxpunkty;
	}

}
